<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220425084612 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE prof DROP CONSTRAINT fk_5bba70bba832c1c9');
        $this->addSql('DROP INDEX idx_5bba70bba832c1c9');
        $this->addSql('ALTER TABLE prof RENAME COLUMN email_id TO users_id');
        $this->addSql('ALTER TABLE prof ADD CONSTRAINT FK_5BBA70BB67B3B43D FOREIGN KEY (users_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5BBA70BB67B3B43D ON prof (users_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE prof DROP CONSTRAINT FK_5BBA70BB67B3B43D');
        $this->addSql('DROP INDEX UNIQ_5BBA70BB67B3B43D');
        $this->addSql('ALTER TABLE prof RENAME COLUMN users_id TO email_id');
        $this->addSql('ALTER TABLE prof ADD CONSTRAINT fk_5bba70bba832c1c9 FOREIGN KEY (email_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_5bba70bba832c1c9 ON prof (email_id)');
    }
}
