<?php

namespace App\Entity;

use ApiPlatform\Core\Action\NotFoundAction;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\MeController;
use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
#[ApiResource(
    normalizationContext: ['groups' => ['read:User:collection']],
    collectionOperations: [
        'post',
        'get',
        'me' => [
            'pagination_enabled' => false,
            'path' => '/me',
            'status' => 200,
            'method' => 'get',
            'controller' => MeController::class,
            'read' => false,
            'openapi_context' => [
                'security' => [['bearerAuth' => []]]
            ]
        ]
    ],
    itemOperations: [
        // commenter ce get et decommenter l'autre si probleme
        // 'get' => [
        //     'controller' => NotFoundAction::class,
        //     'openapi_context' => ['summary' => 'hidden'],
        //     'read' => false,
        //     'output' => false
        // ],
        'get' => [
            'normalization_context' => ['groups' => ['read:User:collection', 'read:User:item']]
        ],
        'put',
        'delete'
    ]
)]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read:User:collection'])]
    private $id;

    #[ORM\Column(type: 'string', length: 180, unique: true)]
    #[Groups(['read:User:collection', 'read:prof:collection', 'read:eleve:collection'])]
    private $email;

    #[ORM\Column(type: 'json')]
    #[Groups(['read:User:collection'])]
    private $roles = [];

    #[ORM\Column(type: 'string')]
    private $password;

    #[ORM\OneToOne(mappedBy: 'users', targetEntity: Prof::class, cascade: ['persist', 'remove'])]
    #[Groups(['read:User:collection'])]
    private $prof;

    #[ORM\OneToOne(mappedBy: 'users', targetEntity: Eleve::class, cascade: ['persist', 'remove'])]
    #[Groups(['read:User:collection'])]
    private $eleve;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getProf(): ?Prof
    {
        return $this->prof;
    }

    public function setProf(?Prof $prof): self
    {
        // unset the owning side of the relation if necessary
        if ($prof === null && $this->prof !== null) {
            $this->prof->setUsers(null);
        }

        // set the owning side of the relation if necessary
        if ($prof !== null && $prof->getUsers() !== $this) {
            $prof->setUsers($this);
        }

        $this->prof = $prof;

        return $this;
    }

    public function getEleve(): ?Eleve
    {
        return $this->eleve;
    }

    public function setEleve(?Eleve $eleve): self
    {
        // unset the owning side of the relation if necessary
        if ($eleve === null && $this->eleve !== null) {
            $this->eleve->setUsers(null);
        }

        // set the owning side of the relation if necessary
        if ($eleve !== null && $eleve->getUsers() !== $this) {
            $eleve->setUsers($this);
        }

        $this->eleve = $eleve;

        return $this;
    }
}
