<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\EleveRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: EleveRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['read:eleve:collection']],
    itemOperations: [
        'put',
        'patch',
        'delete',
        'get' => [
            'normalization_context' => ['groups' => ['read:eleve:collection', 'read:eleve:item']]
        ]
    ],
    collectionOperations: [
        'post',
        'get' => [
            'pagination_enabled' => false
        ]
    ]
)]
class Eleve
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read:eleve:collection', 'read:cours:collection', 'read:User:collection'])]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['read:eleve:collection', 'read:cours:collection', 'read:User:collection'])]
    private $firstname;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['read:eleve:collection', 'read:cours:collection', 'read:User:collection'])]
    private $lastname;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['read:eleve:collection', 'read:cours:collection', 'read:User:collection'])]
    private $city;

    #[ORM\Column(type: 'integer')]
    #[Groups(['read:eleve:collection', 'read:User:collection'])]
    private $streetnumber;

    #[ORM\Column(type: 'integer')]
    #[Groups(['read:eleve:collection', 'read:User:collection'])]
    private $zipcode;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['read:eleve:collection', 'read:User:collection'])]
    private $street;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['read:eleve:collection', 'read:cours:collection'])]
    private $email;

    #[ORM\OneToOne(inversedBy: 'eleve', targetEntity: User::class, cascade: ['persist', 'remove'])]
    #[Groups(['read:eleve:collection'])]
    private $users;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getStreetnumber(): ?int
    {
        return $this->streetnumber;
    }

    public function setStreetnumber(int $streetnumber): self
    {
        $this->streetnumber = $streetnumber;

        return $this;
    }

    public function getZipcode(): ?int
    {
        return $this->zipcode;
    }

    public function setZipcode(int $zipcode): self
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUsers(): ?User
    {
        return $this->users;
    }

    public function setUsers(?User $users): self
    {
        $this->users = $users;

        return $this;
    }
}
