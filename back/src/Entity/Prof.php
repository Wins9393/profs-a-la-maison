<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProfRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ProfRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['read:prof:collection']],
    itemOperations: [
        'put',
        'patch',
        'delete',
        'get' => [
            'normalization_context' => ['groups' => ['read:prof:collection', 'read:prof:item']],
        ]
    ],
    collectionOperations: [
        'post',
        'get' => [
            'pagination_enabled' => false
        ]
    ]
)]
class Prof
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read:prof:collection', 'read:cours:collection', 'read:User:collection'])]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['read:prof:collection', 'read:cours:collection', 'read:User:collection'])]
    private $firstname;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['read:prof:collection', 'read:cours:collection', 'read:User:collection'])]
    private $lastname;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['read:prof:collection', 'read:cours:collection', 'read:User:collection'])]
    private $city;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['read:prof:collection', 'read:User:collection'])]
    private $street;

    #[ORM\Column(type: 'integer')]
    #[Groups(['read:prof:collection', 'read:User:collection'])]
    private $streetnumber;

    #[ORM\Column(type: 'float')]
    #[Groups(['read:prof:collection', 'read:User:collection'])]
    private $pricing;

    #[ORM\Column(type: 'array', nullable: true)]
    #[Groups(['read:prof:collection', 'read:User:collection', 'read:cours:collection'])]
    private $note = [];

    #[ORM\Column(type: 'integer')]
    #[Groups(['read:prof:collection', 'read:User:collection'])]
    private $zipcode;

    #[ORM\ManyToMany(targetEntity: Matiere::class, inversedBy: 'profs')]
    #[Groups(['read:prof:collection', 'read:matiere:collection', 'read:User:collection'])]
    private $matiere;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['read:prof:collection', 'read:User:collection'])]
    private $image;

    #[ORM\OneToOne(inversedBy: 'prof', targetEntity: User::class, cascade: ['persist', 'remove'])]
    #[Groups(['read:prof:collection'])]
    private $users;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['read:prof:collection', 'read:cours:collection'])]
    private $email;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['read:prof:collection', 'read:User:collection'])]
    private $description;

    public function __construct()
    {
        $this->matiere = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getStreetnumber(): ?int
    {
        return $this->streetnumber;
    }

    public function setStreetnumber(int $streetnumber): self
    {
        $this->streetnumber = $streetnumber;

        return $this;
    }

    public function getPricing(): ?float
    {
        return $this->pricing;
    }

    public function setPricing(float $pricing): self
    {
        $this->pricing = $pricing;

        return $this;
    }

    public function getNote(): ?array
    {
        return $this->note;
    }

    public function setNote(?array $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getZipcode(): ?int
    {
        return $this->zipcode;
    }

    public function setZipcode(int $zipcode): self
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * @return Collection<int, Matiere>
     */
    public function getMatiere(): Collection
    {
        return $this->matiere;
    }

    public function addMatiere(Matiere $matiere): self
    {
        if (!$this->matiere->contains($matiere)) {
            $this->matiere[] = $matiere;
        }

        return $this;
    }

    public function removeMatiere(Matiere $matiere): self
    {
        $this->matiere->removeElement($matiere);

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getUsers(): ?User
    {
        return $this->users;
    }

    public function setUsers(?User $users): self
    {
        $this->users = $users;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
