<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\MatiereRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: MatiereRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['read:matiere:collection']],
    collectionOperations: [
        'get' => [
            'pagination_enabled' => false
        ],
        'post'
    ],
    itemOperations: [
        'put',
        'delete',
        'get' => [
            'normalization_context' => ['groups' => ['read:matiere:collection', 'read:matiere:item']]
        ]
    ]
)]
class Matiere
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read:matiere:collection', 'read:cours:collection', 'read:prof:collection'])]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[ApiFilter(SearchFilter::class, strategy: 'ipartial')]
    #[Groups(['read:matiere:collection', 'read:cours:collection', 'read:prof:collection'])]
    private $name;

    // #[ORM\ManyToMany(targetEntity: Prof::class, mappedBy: 'matiere')]
    // #[Groups(['read:matiere:item'])]
    // private $profs;

    public function __construct()
    {
        // $this->profs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Prof>
     */
    // public function getProfs(): Collection
    // {
    //     return $this->profs;
    // }

    // public function addProf(Prof $prof): self
    // {
    //     if (!$this->profs->contains($prof)) {
    //         $this->profs[] = $prof;
    //         $prof->addMatiere($this);
    //     }

    //     return $this;
    // }

    // public function removeProf(Prof $prof): self
    // {
    //     if ($this->profs->removeElement($prof)) {
    //         $prof->removeMatiere($this);
    //     }

    //     return $this;
    // }
}
