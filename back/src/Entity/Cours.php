<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CoursRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: CoursRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['read:cours:collection']],
    itemOperations: [
        'put',
        'patch',
        'delete',
        'get' => [
            'normalization_context' => ['groups' => ['read:cours:collection', 'read:cours:item']]
        ]
    ],
    collectionOperations: [
        'post',
        'get' => [
            'pagination_enabled' => false
        ]
    ]
)]
class Cours
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read:cours:collection'])]
    private $id;

    #[ORM\ManyToOne(targetEntity: Prof::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['read:cours:collection'])]
    private $prof;

    #[ORM\ManyToOne(targetEntity: Eleve::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['read:cours:collection'])]
    private $eleve;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['read:cours:collection'])]
    private $date;

    #[ORM\ManyToOne(targetEntity: Matiere::class)]
    //#[ORM\JoinColumn(nullable: false)]
    #[Groups(['read:cours:collection'])]
    private $matiere;

    #[ORM\Column(type: 'boolean', nullable: true)]
    #[Groups(['read:cours:collection'])]
    private $isNoted = false;

    #[ORM\Column(type: 'boolean', nullable: true)]
    #[Groups(['read:cours:collection'])]
    private $isSeen = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProf(): ?Prof
    {
        return $this->prof;
    }

    public function setProf(?Prof $prof): self
    {
        $this->prof = $prof;

        return $this;
    }

    public function getEleve(): ?Eleve
    {
        return $this->eleve;
    }

    public function setEleve(?Eleve $eleve): self
    {
        $this->eleve = $eleve;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getMatiere(): ?Matiere
    {
        return $this->matiere;
    }

    public function setMatiere(?Matiere $matiere): self
    {
        $this->matiere = $matiere;

        return $this;
    }

    public function getIsNoted(): ?bool
    {
        return $this->isNoted;
    }

    public function setIsNoted(?bool $isNoted): self
    {
        $this->isNoted = $isNoted;

        return $this;
    }

    public function isIsSeen(): ?bool
    {
        return $this->isSeen;
    }

    public function setIsSeen(?bool $isSeen): self
    {
        $this->isSeen = $isSeen;

        return $this;
    }
}
