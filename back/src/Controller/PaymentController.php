<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Stripe\Stripe;
use Stripe\Checkout\Session;
use Stripe\PaymentIntent;

class PaymentController extends AbstractController
{
    #[Route('/checkout', name: 'checkout')]
    public function checkout($stripeSK, $front_url, Request $request): Response
    {
        Stripe::setApiKey($stripeSK);

        $session = Session::create([
            'line_items' => [[
                'price_data' => [
                    'currency' => 'eur',

                    'product_data' => [
                        'name' => "1h de cours avec {$request->query->get('name')}",

                    ],
                    'unit_amount' => $request->query->get('pricing') * 100 ? $request->query->get('pricing') * 100 : 1099,
                ],
                'quantity' => 1,
            ]],
            'mode' => 'payment',
            'success_url' => "{$front_url}/success?session_id={CHECKOUT_SESSION_ID}",
            'cancel_url' => $front_url,
            // 'success_url' => "http://localhost:3000/success?session_id={CHECKOUT_SESSION_ID}",
            // 'cancel_url' => "http://localhost:3000",
            'payment_intent_data' => [
                'application_fee_amount' => ($request->query->get('pricing') * 100) * 0.05,
                'transfer_data' => [
                    'destination' => $request->query->get('stripeId')
                ]
            ]
        ]);
        // dd($session);

        return $this->redirect($session->url, 303);
    }

    #[Route('/order-info', name: 'order-info')]
    public function getOrderInfo($stripeSK, Request $request): Response
    {
        Stripe::setApiKey($stripeSK);

        $session = Session::retrieve($request->get('session_id'));

        echo json_encode(array($session));

        return new Response();
    }

    #[Route('/create-stripe-account', name: 'create-stripe-account')]
    public function createStripeAccount($stripeSK, Request $request, $front_url, $back_url): Response
    {
        // dd($request->get("email"));
        $stripe = new \Stripe\StripeClient($stripeSK);
        $account = $stripe->accounts->create([
            'type' => 'express',
            'country' => 'FR',
            'email' => $request->get('email'),
            'capabilities' => [
                'card_payments' => ['requested' => true],
                'transfers' => ['requested' => true],
            ],
            'business_type' => 'individual',
            'business_profile' => ['product_description' => 'Une heure de cours à domicile'],
        ]);

        // dd($account);

        $accountLink = $stripe->accountLinks->create(
            [
                'account' => $account->id,
                // 'refresh_url' => 'http://localhost:8000/create-stripe-account',
                // 'return_url' => 'http://localhost:3000',
                'refresh_url' => "{$back_url}/create-stripe-account",
                'return_url' => $front_url,
                'type' => 'account_onboarding',
            ]
        );
        // dd($accountLink);
        return $this->redirect($accountLink->url, 303);
    }

    #[Route('/stripe-account-infos', name: 'stripe-account-infos')]
    public function getStripeAccountInfos($stripeSK)
    {
        $stripe = new \Stripe\StripeClient($stripeSK);
        $stripeAccounts = $stripe->accounts->all(['limit' => 100]);

        // dd($stripeAccounts);
        // echo json_encode(array($stripeAccounts));

        return new Response(json_encode($stripeAccounts));
    }


    #[Route('/stripe_webhook', name: 'stripe_webhook')]
    public function webhook($stripeSK): Response
    {
        Stripe::setApiKey($stripeSK);

        $payload = @file_get_contents('php://input');
        $event = null;

        try {
            $event = \Stripe\Event::constructFrom(
                json_decode($payload, true)
            );
            dd($event);
        } catch (\UnexpectedValueException $e) {
            // Invalid payload
            http_response_code(400);
            exit();
        }

        // Handle the event
        switch ($event->type) {
            case 'payment_intent.succeeded':
                $paymentIntent = $event->data->object; // contains a \Stripe\PaymentIntent
                // Then define and call a method to handle the successful payment intent.
                // handlePaymentIntentSucceeded($paymentIntent);
                dd($paymentIntent);
                break;
            case 'payment_method.attached':
                $paymentMethod = $event->data->object; // contains a \Stripe\PaymentMethod
                // Then define and call a method to handle the successful attachment of a PaymentMethod.
                // handlePaymentMethodAttached($paymentMethod);
                break;
                // ... handle other event types
            default:
                echo 'Received unknown event type ' . $event->type;
        }

        http_response_code(200);

        return new Response();
    }
};
