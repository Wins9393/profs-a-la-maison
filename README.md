# Getting Started with Create React App

* Symfony 6
* React
* API Platform
* Stripe

## How to install the project

First of you need to install the dependencies
by using the two following commands.

For the front, run it from the front folder : `npm install`

For the back, run it from the back folder : `composer install`

## How to launch the project

To launch the front, you can run : `npm run-script start`

Open [http://localhost:3000](http://localhost:3000) to view it in your browser.
