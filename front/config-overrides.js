module.exports = function override(config, env) {
  let loaders = config.resolve;
  loaders.fallback = {
    path: require.resolve("path-browserify"),
    querystring: require.resolve("querystring-es3"),
  };
  return config;
};
