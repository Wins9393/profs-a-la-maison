import { Form, Input, Button } from "antd";
import { useContext, useEffect } from "react";
import { Navigate } from "react-router-dom";
import MainContext from "../../contexts/MainContext";
import InfoMessages from "../Account/InfoMessages";
import "./login.css";

const LoginForm = () => {
  const { currentUser, login, message, setMessage } = useContext(MainContext);

  useEffect(() => {
    setMessage({});
  }, []);

  const onFinish = async (values) => {
    login(values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return !currentUser ? (
    <div className="container-connexion-form">
      <h2 className="connexion-title">Connexion</h2>
      <Form
        className="connexion-form"
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="Email"
          name="username"
          rules={[
            {
              required: true,
              type: "string",
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        {message ? <InfoMessages mess={message} /> : ""}

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button type="primary" htmlType="submit">
            Connexion
          </Button>
        </Form.Item>
      </Form>
    </div>
  ) : (
    <Navigate
      to={{
        pathname: "/",
      }}
    />
  );
};

export default LoginForm;
