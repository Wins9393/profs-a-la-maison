const Logout = () => {
  const logoutApi = async () => {
    const res = await fetch(`${process.env.REACT_APP_API_ENTRYPOINT}/logout`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    });

    const data = await res.json();
  };
};

export default Logout;
