import { useContext, useEffect, useState } from "react";
import { Carousel } from "antd";
import MainContext from "../../contexts/MainContext";
import { Link } from "react-router-dom";
import "./bestProfs.css";
import ProfCard from "../ProfCard/ProfCard";
import { Rings } from "react-loader-spinner";

const BestProfs = () => {
  const { profs } = useContext(MainContext);
  const [bestProfs, setBestProfs] = useState([]);

  useEffect(() => {
    findBestProfs();
  }, [profs]);

  const findBestProfs = () => {
    let bestProfsTmp = [];

    const compare = (a, b) => {
      let aNote = a.note.reduce((pv, cv) => pv + cv, 0);
      let bNote = b.note.reduce((pv, cv) => pv + cv, 0);

      if (aNote / a.note.length < bNote / b.note.length) return 1;
      if (aNote / a.note.length > bNote / b.note.length) return -1;
      return 0;
    };
    bestProfsTmp = profs.sort(compare).slice(0, 3);

    setBestProfs(bestProfsTmp);
  };

  if (profs.length) {
    return (
      <div className="best-profs-container">
        <div>
          <center>
            <h2 className="best-profs-title">Les meilleurs profs</h2>
          </center>
        </div>
        <Carousel autoplay className="best-profs-carousel">
          {bestProfs.map(({ ...prof }) => {
            return (
              <div className="best-profs-wrapper" key={prof.id}>
                <div className="card-wrapper">
                  <Link
                    className="prof-card-best-profs"
                    to={`/prof/${prof.id}`}
                  >
                    <ProfCard {...prof} />
                  </Link>
                </div>
              </div>
            );
          })}
        </Carousel>
      </div>
    );
  } else {
    return (
      <div className="loader-container-component">
        <Rings height="100" width="100" color="#fbc531" ariaLabel="loading" />
      </div>
    );
  }
};

export default BestProfs;
