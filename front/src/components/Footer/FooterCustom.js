import { Divider } from "antd";
import {
  FacebookOutlined,
  InstagramOutlined,
  TwitterCircleFilled,
  LinkedinOutlined,
} from "@ant-design/icons";
import "./footer.css";

const FooterCustom = () => {
  return (
    <div className="footer">
      <div className="footer-first-section">
        <div className="footer-section">
          <h3>A propos</h3>
          <a href="#">Qui sommes-nous ?</a>
          <a href="#">Nos valeurs</a>
          <a href="#">Mentions légales</a>
          <a href="#">Confidentialité</a>
        </div>
        <div className="footer-section">
          <h3>Assistance</h3>
          <a href="#">Contact</a>
        </div>
        <div className="footer-section">
          <h3>Suivez-nous</h3>
          <div className="display-flex-inline">
            <a href="#">
              <FacebookOutlined className="social-network-icon" />
            </a>
            <a href="#">
              <InstagramOutlined className="social-network-icon" />
            </a>
            <a href="#">
              <TwitterCircleFilled className="social-network-icon" />
            </a>
            <a href="#">
              <LinkedinOutlined className="social-network-icon" />
            </a>
          </div>
        </div>
      </div>
      <div className="footer-second-section">
        <Divider style={{ backgroundColor: "#fbc531" }} />
        <div>
          <span className="nom-site">©ProfsADom</span>{" "}
          {new Date().getFullYear()} Tous droits réservés
        </div>
      </div>
    </div>
  );
};

export default FooterCustom;
