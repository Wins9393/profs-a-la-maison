import "./infoWindow.css";
import { Button, Rate } from "antd";
import { useContext, useEffect, useState } from "react";
import MainContext from "../../contexts/MainContext";

const InfoWindow = (props) => {
  const { prof, currentProfClicked } = props;
  const { getAverage } = useContext(MainContext);
  const [averageMap, setAverageMap] = useState(0);

  const divPriceBtnStyle = {
    transition: "1s",
    height: currentProfClicked[0] ? "32px" : "0",
  };

  useEffect(() => {
    setAverageMap(getAverage(prof));
  });

  return (
    <div className="infoWindow-container">
      <div className="img-infoWindow-container">
        <div className="round-shape-infoWindow">
          <img width={96} src={prof.image} alt="image du prof" />
        </div>
      </div>

      <div style={{ fontSize: 16 }}>
        {prof.firstname} {prof.lastname}
      </div>
      {averageMap ? <Rate allowHalf disabled defaultValue={averageMap} /> : ""}

      <div style={{ fontSize: 14, color: "grey" }}>{prof.matiere[0].name}</div>
      {prof.matiere[1] ? (
        <div style={{ fontSize: 14, color: "grey" }}>
          {prof.matiere[1].name}
        </div>
      ) : (
        ""
      )}
      <div className="price-button-container" style={divPriceBtnStyle}>
        <div style={{ fontSize: 14, color: "grey" }}>{prof.pricing}€</div>
        <Button
          type="primary"
          shape="round"
          size="small"
          href={`/prof/${prof.id}`}
        >
          Plus d'infos
        </Button>
      </div>
    </div>
  );
};

export default InfoWindow;
