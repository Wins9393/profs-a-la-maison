import { useState, useEffect } from "react";

const AllEleves = () => {
  const [elevesApi, setElevesApi] = useState([]);

  useEffect(() => {
    fetchEleves();
  }, []);

  const fetchEleves = async () => {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_ENTRYPOINT}/eleves?page=1`,
        {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
        }
      );

      const data = await response.json();

      setElevesApi(data);
    } catch (err) {
      throw err;
    }
  };

  if (elevesApi.length) {
    return (
      <div className="App">
        <header className="App-header"></header>
      </div>
    );
  } else {
    return (
      <div className="App">
        <header className="App-header">
          <h1>Chargement...</h1>
        </header>
      </div>
    );
  }
};

export default AllEleves;
