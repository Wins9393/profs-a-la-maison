import { useContext, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import MainContext from "../../contexts/MainContext";
import { Row, Col, Avatar, Rate, Button, DatePicker, Modal } from "antd";
import { CalendarOutlined } from "@ant-design/icons";
import "./profById.css";
import InfoMessages from "../Account/InfoMessages";
import moment from "moment";
import "moment/locale/fr";
import locale from "antd/es/date-picker/locale/fr_FR";
import { Rings } from "react-loader-spinner";

const ProfById = () => {
  const { id } = useParams();
  const {
    fetchProfById,
    profById,
    loading,
    setLoading,
    message,
    setMessage,
    currentUserAll,
    setCurrentUserAll,
    average,
    cours,
    getStripeAccount,
    stripeId,
    setStripeId,
  } = useContext(MainContext);

  const [token, setToken] = useState(null);
  const [visible, setVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [dateCours, setDateCours] = useState("");
  const [selectedDate, setSelectedDate] = useState();
  let disabledHoursTab = [];

  useEffect(() => {
    setLoading(true);
  }, []);

  useEffect(() => {
    fetchProfById(id);
    setMessage({});
  }, [loading]);

  useEffect(() => {
    setStripeId(null);
    getStripeAccount(profById);
  }, [profById]);

  useEffect(() => {
    getToken();
    fetchCurrentUser();
  }, [token]);

  const showModal = () => {
    setVisible(true);
  };

  const handleOkModal = async () => {
    setConfirmLoading(true);

    if (dateCours) {
      let coursTmp = {
        prof: `/api/profs/${profById.id}`,
        eleve: `/api/eleves/${currentUserAll.eleve.id}`,
        date: dateCours,
        matiere: `/api/matieres/${profById.matiere[0].id}`,
      };

      if (coursTmp) {
        localStorage.setItem("nextCours", JSON.stringify(coursTmp));
      }

      if (stripeId) {
        window.location.replace(
          `${process.env.REACT_APP_BACK_URL}/checkout?name=${profById.firstname} ${profById.lastname}&pricing=${profById.pricing}&stripeId=${stripeId}`
          // `https://localhost:8000/checkout?name=${profById.firstname} ${profById.lastname}&pricing=${profById.pricing}&stripeId=${stripeId}`
        );
      }
    } else {
      setMessage({
        error: `Date ou heure invalide`,
      });
    }

    setVisible(false);
    setConfirmLoading(false);
  };

  const handleOkDatePicker = (value) => {
    let checkHourValue = moment(value)
      .format("DD/MM/YYYY HH:mm")
      .split(" ")[1]
      .split(":")[0];

    if (checkHourValue[0] == 0) checkHourValue = checkHourValue[1];

    if (
      checkHourValue >= 8 &&
      checkHourValue <= 17 &&
      !disabledHoursTab.includes(parseInt(checkHourValue))
    )
      setDateCours(value.toDate());
    else
      setMessage({
        error: `Veuillez séléctionner une heure valide`,
      });
  };

  const handleCancel = () => {
    setVisible(false);
  };

  function range(start, end) {
    const result = [];
    for (let i = start; i < end; i++) {
      result.push(i);
    }
    return result;
  }

  function disabledDate(current) {
    return current && current < moment().endOf("day");
  }

  function disabledDateTime() {
    let disabledHour;
    disabledHoursTab = [];

    cours.map((oneCours) => {
      if (profById.id === oneCours.prof.id) {
        const dateBdd = moment(oneCours.date)
          .format("DD/MM/YYYY HH:mm")
          .split(" ");

        if (selectedDate)
          if (
            dateBdd[0] == selectedDate.format("DD/MM/YYYY HH:mm").split(" ")[0]
          ) {
            if (dateBdd[1].split(":")[0].split("")[0] == 0)
              disabledHour = dateBdd[1].split(":")[0].split("")[1];
            else disabledHour = dateBdd[1].split(":")[0];

            disabledHoursTab.push(parseInt(disabledHour));
          }
      }
    });

    if (disabledHoursTab.length) {
      return {
        disabledHours: () => {
          return [...range(0, 8), ...disabledHoursTab, ...range(18, 24)];
        },
      };
    } else {
      return {
        disabledHours: () => {
          return [...range(0, 8), ...range(18, 24)];
        },
      };
    }
  }

  const getToken = () => {
    if (localStorage.getItem("currentUser")) {
      setToken(JSON.parse(localStorage.getItem("currentUser")).token);
    }
  };

  const fetchCurrentUser = async () => {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_ENTRYPOINT}/me`,
        {
          method: "GET",
          headers: {
            Authorization: `Bearer ${token}`,
            Accept: "application/json",
            "Content-Type": "application/json",
          },
        }
      );

      const data = await response.json();

      if (data.id) {
        setCurrentUserAll(data);
      }

      if (currentUserAll !== []) {
        if (data.message === "Expired JWT Token") {
          localStorage.removeItem("currentUser");
          setCurrentUserAll(null);
          setMessage({ error: "Token expiré, veuillez vous reconnecter !" });
          window.location.replace("/");
        }
      }
    } catch (err) {
      throw err;
    }
  };

  const onClickRdv = () => {
    if (currentUserAll.id) {
      if (currentUserAll.roles.includes("ROLE_ELEVE")) {
        showModal();
      } else {
        setMessage({
          error:
            "Vous devez être connecté avec un compte élève pour prendre rendez-vous",
        });
      }
    } else {
      setMessage({
        error:
          "Vous devez être connecté avec un compte élève pour prendre rendez-vous",
      });
    }
  };

  return loading ? (
    <div className="loader-container">
      <Rings height="100" width="100" color="#fbc531" ariaLabel="loading" />
    </div>
  ) : (
    <div className="container-prof-by-id">
      <Row className="row-container">
        <Col xs={0} sm={0} md={0} lg={2} xl={4}></Col>
        <Col
          className="colGeneral col-gauche"
          xs={24}
          sm={24}
          md={24}
          lg={6}
          xl={4}
        >
          <div className="square-identity">
            <h2 className="title-infos-page">
              {profById.firstname} {profById.lastname}
            </h2>
            <Avatar size={148} src={profById.image} />
            {average != 0 ? (
              <Rate allowHalf disabled defaultValue={average} />
            ) : (
              ""
            )}
          </div>
          <div className="square-location">
            <h2 className="title-infos-page">Localisation:</h2>
            <div>
              <h3>
                {profById.streetnumber}, {profById.street} {profById.zipcode}{" "}
                {profById.city}
              </h3>
            </div>
          </div>
          <div className="square-price">
            <h2 className="title-infos-page">Tarifs:</h2>
            <div>
              <h3>{profById.pricing}€ /heure</h3>
            </div>
          </div>
          {stripeId ? (
            <div className="square-rdv">
              <Button
                type="primary"
                icon={<CalendarOutlined />}
                block
                className="btn-rdv"
                onClick={onClickRdv}
              >
                Prendre RDV
              </Button>
              <Modal
                title="Choisissez la date du rendez-vous"
                visible={visible}
                onOk={handleOkModal}
                confirmLoading={confirmLoading}
                onCancel={handleCancel}
              >
                <DatePicker
                  showNow={false}
                  locale={locale}
                  size="large"
                  format="DD/MM/YYYY HH:mm:ss"
                  disabledDate={disabledDate}
                  disabledTime={disabledDateTime}
                  onSelect={setSelectedDate}
                  showTime={{
                    defaultValue: moment("08:00", "HH:mm"),
                    format: "HH",
                    // minuteStep: 30,
                    hideDisabledOptions: false,
                  }}
                  onOk={handleOkDatePicker}
                />
              </Modal>
            </div>
          ) : (
            <div className="loader-container-component">
              <Rings
                height="100"
                width="100"
                color="#fbc531"
                ariaLabel="loading"
              />
            </div>
          )}

          {message ? <InfoMessages mess={message} /> : ""}
        </Col>
        <Col
          className="colGeneral col-droite"
          xs={24}
          sm={24}
          md={24}
          lg={14}
          xl={12}
        >
          <div className="rectangle-matieres">
            <h2 className="title-infos-page">Matières enseignées:</h2>
            <div className="matieres-container">
              {profById.matiere ? (
                profById.matiere.map(({ ...mat }) => {
                  return (
                    <h3 key={mat.id} className="matiere-item">
                      {mat.name}
                    </h3>
                  );
                })
              ) : (
                <div></div>
              )}
            </div>
          </div>
          <div className="rectangle-description">
            <h2 className="title-infos-page">
              Description {profById.firstname}:
            </h2>
            <div className="prof-description">
              <p>{profById.description ? profById.description : ""}</p>
            </div>
          </div>
        </Col>
        <Col xs={0} sm={0} md={0} lg={2} xl={4}></Col>
      </Row>
    </div>
  );
};

export default ProfById;
