import BestProfs from "../BestProfs/BestProfs";
import Map from "../GoogleMap/Map";
import { Carousel } from "antd";
import { Rings } from "react-loader-spinner";
import "./home.css";
import { useEffect } from "react";

const Home = () => {
  const contentStyle = {
    minWidth: "100%",
  };

  let logo = "/logo-pad.svg";

  return (
    <div className="content-body">
      <div className="profs-a-dom">
        {logo ? (
          <img className="logo-profs-a-dom" src={logo} alt="logo-profsadom" />
        ) : (
          <div className="loader-container-component">
            <Rings
              height="100"
              width="100"
              color="#fbc531"
              ariaLabel="loading"
            />
          </div>
        )}
      </div>
      <Carousel autoplay effect="fade">
        <div className="container-home-carousel">
          <img
            style={contentStyle}
            src="https://images.unsplash.com/photo-1521791136064-7986c2920216?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2938&q=80"
            alt="eleve seul"
          />
        </div>
        <div className="container-home-carousel">
          <img
            style={contentStyle}
            src="https://images.unsplash.com/photo-1600880292203-757bb62b4baf?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2940&q=80"
            alt="poing eleve et prof"
          />
        </div>

        <div className="container-home-carousel">
          <img
            style={contentStyle}
            src="https://www.officeforstudents.org.uk/media/27fa8ee5-bb4b-4246-a874-e34037a777c3/learning-during-lockdown.jpg"
            alt="prof et eleve check"
          />
        </div>
      </Carousel>
      <Map />
      <BestProfs />
    </div>
  );
};

export default Home;
