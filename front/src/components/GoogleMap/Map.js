import "./map.css";
import React from "react";
import { useState, useContext, useEffect } from "react";
import GoogleMapReact from "google-map-react";
import MainContext from "../../contexts/MainContext";
import MarkerMap from "../MarkerMap/MarkerMap";
import Geocode from "react-geocode";
import { Button, Input, Select, Slider } from "antd";

Geocode.setApiKey(`${process.env.REACT_APP_MAP_API_KEY}`);
Geocode.setLanguage("fr");
Geocode.setRegion("fr");
Geocode.setLocationType("ROOFTOP");
Geocode.enableDebug();

const { Search } = Input;
const { Option } = Select;

const Map = () => {
  const {
    fetchProfs,
    profs,
    profsCoords,
    setProfsCoords,
    getProfsCoords,
    matieres,
  } = useContext(MainContext);
  const [center, setCenter] = useState({
    lat: 48.85899588063377,
    lng: 2.3441096046642373,
  });
  const [zoom, setZoom] = useState(10);
  const [currentProfClicked, setCurrentProfClicked] = useState([]);

  useEffect(() => {
    fetchProfs();
  }, []);

  useEffect(() => {
    getProfsCoords(profs);

    const clearIcon = document.querySelector(".ant-input-clear-icon");
    const inputCity =
      document.querySelector(".search-bar-home").firstChild.firstChild
        .firstChild;
    const resetButton = document.querySelector(".button-reset");

    resetButton.addEventListener("click", () => {
      getProfsCoords(profs);
      setZoom(10);
      // Paris
      setCenter(center);
    });

    inputCity.addEventListener("input", (e) => {
      if (e.target.value.length < 2) {
        getProfsCoords(profs);
        setZoom(10);
        // Paris
        setCenter(center);
      }
    });

    clearIcon.addEventListener("click", () => {
      getProfsCoords(profs);
      setZoom(10);
      // Paris
      setCenter(center);
    });
  }, [profs]);

  const marksNote = {
    0: "0/5",
    1: "1/5",
    2: "2/5",
    3: "3/5",
    4: "4/5",
    5: "5/5",
  };

  const markPrix = {
    0: "0€",
    500: "500€",
  };

  const onMarkerMouseEnter = (key) => {
    if (currentProfClicked[0] !== undefined) {
      return;
    }
    setProfsCoords((prevState) => {
      const index = prevState.findIndex((e) => e.id === parseInt(key));
      prevState[index].show = !prevState[index].show;
      return prevState;
    });
  };

  const onMarkerMouseLeave = (key) => {
    if (currentProfClicked[0] !== undefined) {
      return;
    } else {
      setProfsCoords((prevState) => {
        const index = prevState.findIndex((e) => e.id === parseInt(key));
        prevState[index].show = false;

        return prevState;
      });
    }
  };

  const onMarkerClick = (key, props) => {
    if (currentProfClicked[0] !== undefined) {
      if (currentProfClicked[0].id === parseInt(key)) {
        const index = profsCoords.findIndex((e) => e.id === parseInt(key));
        setProfsCoords((prev) => {
          prev[index].show = false;
          return prev;
        });
        setZoom(10);
        setCenter(currentProfClicked.coords);
        setCurrentProfClicked([]);
      }
    }
    profsCoords.map((prof) => {
      if (prof.id === parseInt(key) && prof.show) {
        setCurrentProfClicked(() => {
          return [prof];
        });
        setZoom(12);
        setCenter(prof.coords);
      }
    });
  };

  const findProfsByMatiere = (matiereID) => {
    let profsTmp = [];

    profsCoords.filter((prof) =>
      prof.matiere.map((mat) => {
        if (mat.id == matiereID) {
          profsTmp.push(prof);
        }
      })
    );

    setProfsCoords(profsTmp);
    if (profsTmp.length === 1) {
      setCenter(profsTmp[0].coords);
      setZoom(11);
    } else if (profsTmp.length > 1) {
      setCenter(profsTmp[1].coords);
      setZoom(10);
    }
  };

  const onSelectSearch = (value) => {
    const matiereID = value.split("/")[3];
    findProfsByMatiere(matiereID);
  };

  const onCitySearch = (value) => {
    const profsTmp = profsCoords.filter((prof) => {
      return prof.city.toLowerCase().includes(value.toLowerCase());
    });
    setProfsCoords(profsTmp);
    if (profsTmp.length === 1) {
      setCenter(profsTmp[0].coords);
      setZoom(11);
    } else if (profsTmp.length > 1) {
      setCenter(profsTmp[1].coords);
      setZoom(10);
    }
  };

  const onRateSearch = (value) => {
    const profsTmp = profsCoords.filter((prof) => {
      const initialValue = 0;
      const sumNote = prof.note.reduce(
        (prev, curr) => prev + curr,
        initialValue
      );
      return sumNote / prof.note.length >= value;
    });
    setProfsCoords(profsTmp);
    if (profsTmp.length === 1) {
      setCenter(profsTmp[0].coords);
      setZoom(11);
    } else if (profsTmp.length > 1) {
      setCenter(profsTmp[1].coords);
      setZoom(10);
    }
  };

  const onPriceSearch = (value) => {
    const profsTmp = profsCoords.filter((prof) => {
      return prof.pricing >= value;
    });
    setProfsCoords(profsTmp);
    if (profsTmp.length === 1) {
      setCenter(profsTmp[0].coords);
      setZoom(11);
    } else if (profsTmp.length > 1) {
      setCenter(profsTmp[1].coords);
      setZoom(10);
    }
  };

  if (!profsCoords) {
    return <div>Chargement...</div>;
  } else {
    return (
      <div>
        <div>
          <center>
            <h2 className="map-title">Trouvez vôtre prof</h2>
          </center>
        </div>
        <div className="map-plus-filters-container">
          <div className="map-container">
            <GoogleMapReact
              bootstrapURLKeys={{
                key: "AIzaSyDSkmLP8-9aiJ9gzf2hCaEjltirGMS_aUA",
              }}
              center={center}
              zoom={zoom}
              onChildClick={onMarkerClick}
              onChildMouseEnter={onMarkerMouseEnter}
              hoverDistance={30}
              onChildMouseLeave={onMarkerMouseLeave}
            >
              {profsCoords.map((prof) => {
                return (
                  <MarkerMap
                    key={prof.id}
                    lat={prof.coords.lat}
                    lng={prof.coords.lng}
                    prof={prof}
                    show={prof.show}
                    currentProfClicked={currentProfClicked}
                  />
                );
              })}
            </GoogleMapReact>
          </div>
          <div className="filters-map-container">
            <div className="filters-content">
              <div className="search-city-home filter-module">
                <div>Rechercher par ville</div>
                <Search
                  className="search-bar-home"
                  placeholder="Choisissez une ville"
                  allowClear
                  enterButton="Rechercher"
                  size="large"
                  onSearch={onCitySearch}
                />
              </div>
              <div className="search-matiere-home filter-module">
                <div>Rechercher par matière</div>
                <Select
                  className="search-bar-home"
                  placeholder="Choisissez une matière"
                  onChange={onSelectSearch}
                >
                  {matieres.map((matiere) => {
                    return (
                      <Option
                        value={`/api/matieres/${matiere.id}`}
                        key={matiere.id}
                      >
                        {matiere.name}
                      </Option>
                    );
                  })}
                </Select>
              </div>
              <div className="slider-note-home filter-module">
                <div>Rechercher par note</div>
                <Slider
                  min={0}
                  max={5}
                  marks={marksNote}
                  step={null}
                  defaultValue={3}
                  onAfterChange={onRateSearch}
                />
              </div>
              <div className="slider-prix-home filter-module">
                <div>Rechercher par prix</div>
                <Slider
                  min={0}
                  max={500}
                  marks={markPrix}
                  step={5}
                  defaultValue={50}
                  onAfterChange={onPriceSearch}
                />
              </div>
              <Button className="button-reset" type="primary">
                Réinitialiser
              </Button>
            </div>
          </div>
        </div>
      </div>
    );
  }
};

export default Map;
