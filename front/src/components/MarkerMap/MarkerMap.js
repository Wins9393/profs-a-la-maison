import { CaretDownFilled } from "@ant-design/icons";
import "./markerMap.css";
import InfoWindow from "../InfoWindow/InfoWindow";

const MarkerMap = ({ prof, currentProfClicked }) => {
  const iconStyle = {
    color: prof.show ? "red" : "#2C3A47",
    fontSize: "32px",
  };

  const roundShapeColor = {
    border: prof.show ? "solid 3px red" : "solid 3px #2C3A47",
  };

  return (
    <div className="marker-container">
      <div className="round-shape" style={roundShapeColor}>
        <img className="img-marker" src={prof.image} alt={prof.firstname} />
      </div>
      <CaretDownFilled style={iconStyle} />
      {prof.show && (
        <InfoWindow prof={prof} currentProfClicked={currentProfClicked} />
      )}
    </div>
  );
};

export default MarkerMap;
