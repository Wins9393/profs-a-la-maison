import {
  HydraAdmin,
  fetchHydra,
  hydraDataProvider,
  ResourceGuesser,
  ListGuesser,
  ShowGuesser,
  FieldGuesser,
  CreateGuesser,
  EditGuesser,
  InputGuesser,
} from "@api-platform/admin";
import { parseHydraDocumentation } from "@api-platform/api-doc-parser";
import { useContext, useEffect } from "react";
import {
  TextField,
  TextInput,
  ReferenceField,
  AutocompleteArrayInput,
  ReferenceArrayInput,
  FunctionField,
  DateField,
  DateTimeInput,
  ReferenceInput,
  AutocompleteInput,
  PasswordInput,
  SelectArrayInput,
} from "react-admin";
import MainContext from "../../contexts/MainContext";

const entrypoint = `${process.env.REACT_APP_API_ENTRYPOINT}`;

const dataProvider = hydraDataProvider({
  entrypoint,
  httpClient: fetchHydra,
  apiDocumentationParser: parseHydraDocumentation,
  mercure: false,
  useEmbedded: false,
});

// CRUD PROF
const ProfsList = (props) => (
  <ListGuesser {...props}>
    <FieldGuesser source={"firstname"} />
    <FieldGuesser source={"lastname"} />
    <FieldGuesser source={"email"} />
    <FieldGuesser source={"streetnumber"} />
    <FieldGuesser source={"street"} />
    <FieldGuesser source={"zipcode"} />
    <FieldGuesser source={"city"} />
    <FieldGuesser source={"pricing"} />

    <ReferenceField
      label="Matiere1"
      source="matiere[0]"
      reference="matieres"
      link="show"
    >
      <TextField source="name" />
    </ReferenceField>

    <ReferenceField
      label="Matiere2"
      source="matiere[1]"
      reference="matieres"
      link="show"
    >
      <TextField source="name" />
    </ReferenceField>
  </ListGuesser>
);

const ProfShow = (props) => (
  <ShowGuesser {...props}>
    <FieldGuesser source={"firstname"} />
    <FieldGuesser source={"lastname"} />
    <FieldGuesser source={"email"} />
    <FieldGuesser source={"streetnumber"} />
    <FieldGuesser source={"street"} />
    <FieldGuesser source={"zipcode"} />
    <FieldGuesser source={"city"} />

    <ReferenceField
      label="Matiere1"
      source="matiere[0]"
      reference="matieres"
      link="show"
    >
      <TextField source="name" />
    </ReferenceField>

    <ReferenceField
      label="Matiere2"
      source="matiere[1]"
      reference="matieres"
      link="show"
    >
      <TextField source="name" />
    </ReferenceField>

    <FieldGuesser source={"pricing"} />
    <FieldGuesser source={"image"} />
    <FieldGuesser source={"description"} />
  </ShowGuesser>
);

const ProfEdit = (props) => (
  <EditGuesser {...props}>
    <InputGuesser source={"firstname"} />
    <InputGuesser source={"lastname"} />
    <InputGuesser source={"email"} />
    <InputGuesser source={"streetnumber"} />
    <InputGuesser source={"street"} />
    <InputGuesser source={"zipcode"} />
    <InputGuesser source={"city"} />

    <ReferenceArrayInput
      source="matiere"
      reference="matieres"
      label="Matiere"
      filterToQuery={(searchText) => ({ name: searchText })}
    >
      <AutocompleteArrayInput optionText="name" />
    </ReferenceArrayInput>

    <InputGuesser source={"pricing"} />
    <InputGuesser source={"image"} />
    <TextInput multiline source={"description"} />
  </EditGuesser>
);

const ProfCreate = (props) => (
  <CreateGuesser {...props}>
    <InputGuesser source={"firstname"} />
    <InputGuesser source={"lastname"} />
    <InputGuesser source={"email"} />
    <InputGuesser source={"streetnumber"} />
    <InputGuesser source={"street"} />
    <InputGuesser source={"zipcode"} />
    <InputGuesser source={"city"} />

    <ReferenceArrayInput
      source="matiere"
      reference="matieres"
      label="Matiere"
      filterToQuery={(searchText) => ({ name: searchText })}
    >
      <AutocompleteArrayInput optionText="name" />
    </ReferenceArrayInput>

    <InputGuesser source={"pricing"} />
    <InputGuesser source={"image"} />
    <InputGuesser source={"description"} />
  </CreateGuesser>
);

// CRUD ELEVE
const ElevesList = (props) => (
  <ListGuesser {...props}>
    <FieldGuesser source={"firstname"} />
    <FieldGuesser source={"lastname"} />
    <FieldGuesser source={"email"} />
    <FieldGuesser source={"streetnumber"} />
    <FieldGuesser source={"street"} />
    <FieldGuesser source={"zipcode"} />
    <FieldGuesser source={"city"} />
  </ListGuesser>
);

const EleveShow = (props) => (
  <ShowGuesser {...props}>
    <FieldGuesser source={"firstname"} />
    <FieldGuesser source={"lastname"} />
    <FieldGuesser source={"email"} />
    <FieldGuesser source={"streetnumber"} />
    <FieldGuesser source={"street"} />
    <FieldGuesser source={"zipcode"} />
    <FieldGuesser source={"city"} />
  </ShowGuesser>
);

const EleveEdit = (props) => (
  <EditGuesser {...props}>
    <InputGuesser source={"firstname"} />
    <InputGuesser source={"lastname"} />
    <InputGuesser source={"email"} />
    <InputGuesser source={"streetnumber"} />
    <InputGuesser source={"street"} />
    <InputGuesser source={"zipcode"} />
    <InputGuesser source={"city"} />
  </EditGuesser>
);

const EleveCreate = (props) => (
  <CreateGuesser {...props}>
    <InputGuesser source={"firstname"} />
    <InputGuesser source={"lastname"} />
    <InputGuesser source={"email"} />
    <InputGuesser source={"streetnumber"} />
    <InputGuesser source={"street"} />
    <InputGuesser source={"zipcode"} />
    <InputGuesser source={"city"} />
  </CreateGuesser>
);

// CRUD COURS
const CoursList = (props) => (
  <ListGuesser {...props}>
    <ReferenceField label="Prof" source="prof" reference="profs" link="show">
      <FunctionField
        label="Prof"
        render={(prof) => `${prof.firstname} ${prof.lastname}`}
      />
    </ReferenceField>

    <ReferenceField label="Eleve" source="eleve" reference="eleves" link="show">
      <FunctionField
        label="Eleve"
        render={(eleve) => `${eleve.firstname} ${eleve.lastname}`}
      />
    </ReferenceField>

    <ReferenceField
      label="Matiere"
      source="matiere"
      reference="matieres"
      link="show"
    >
      <TextField source="name" />
    </ReferenceField>

    <DateField showTime source={"date"} />
    <FieldGuesser source={"isNoted"} />
  </ListGuesser>
);

const CoursShow = (props) => (
  <ShowGuesser {...props}>
    <ReferenceField label="Prof" source="prof" reference="profs" link="show">
      <FunctionField
        label="Prof"
        render={(prof) => `${prof.firstname} ${prof.lastname}`}
      />
    </ReferenceField>

    <ReferenceField label="Eleve" source="eleve" reference="eleves" link="show">
      <FunctionField
        label="Eleve"
        render={(eleve) => `${eleve.firstname} ${eleve.lastname}`}
      />
    </ReferenceField>

    <ReferenceField
      label="Matiere"
      source="matiere"
      reference="matieres"
      link="show"
    >
      <TextField source="name" />
    </ReferenceField>

    <DateField showTime source={"date"} />
    <FieldGuesser source={"isNoted"} />
  </ShowGuesser>
);

const CoursEdit = (props) => (
  <EditGuesser {...props}>
    <ReferenceInput
      label="Prof"
      source="prof"
      reference="profs"
      filterToQuery={(searchText) => ({ name: searchText })}
    >
      <AutocompleteInput
        optionText={(prof) => `${prof.firstname} ${prof.lastname}`}
      />
    </ReferenceInput>

    <ReferenceInput
      label="Eleve"
      source="eleve"
      reference="eleves"
      filterToQuery={(searchText) => ({ name: searchText })}
    >
      <AutocompleteInput
        optionText={(eleve) => `${eleve.firstname} ${eleve.lastname}`}
      />
    </ReferenceInput>

    <ReferenceInput
      label="Matiere"
      source="matiere"
      reference="matieres"
      filterToQuery={(searchText) => ({ name: searchText })}
    >
      <AutocompleteInput optionText={(matiere) => matiere.name} />
    </ReferenceInput>

    <DateTimeInput source={"date"} />
  </EditGuesser>
);

const CoursCreate = (props) => (
  <CreateGuesser {...props}>
    <ReferenceInput
      label="Prof"
      source="prof"
      reference="profs"
      filterToQuery={(searchText) => ({ name: searchText })}
    >
      <AutocompleteInput
        optionText={(prof) => `${prof.firstname} ${prof.lastname}`}
      />
    </ReferenceInput>

    <ReferenceInput
      label="Eleve"
      source="eleve"
      reference="eleves"
      filterToQuery={(searchText) => ({ name: searchText })}
    >
      <AutocompleteInput
        optionText={(eleve) => `${eleve.firstname} ${eleve.lastname}`}
      />
    </ReferenceInput>

    <ReferenceInput
      label="Matiere"
      source="matiere"
      reference="matieres"
      filterToQuery={(searchText) => ({ name: searchText })}
    >
      <AutocompleteInput optionText={(matiere) => matiere.name} />
    </ReferenceInput>

    <DateTimeInput source={"date"} />
    <InputGuesser source={"isNoted"} />
  </CreateGuesser>
);

// CRUD USER
const UserList = (props) => (
  <ListGuesser {...props}>
    <FieldGuesser source={"email"} />
    <FieldGuesser source={"roles"} />

    <ReferenceField label="Prof" source="prof" reference="profs" link="show">
      <FunctionField
        label="Prof"
        render={(prof) => `${prof.firstname} ${prof.lastname}`}
      />
    </ReferenceField>

    <ReferenceField label="Eleve" source="eleve" reference="eleves" link="show">
      <FunctionField
        label="Eleve"
        render={(eleve) => `${eleve.firstname} ${eleve.lastname}`}
      />
    </ReferenceField>
  </ListGuesser>
);

const UserShow = (props) => (
  <ShowGuesser {...props}>
    <FieldGuesser source={"email"} />
    <FieldGuesser source={"roles"} />

    <ReferenceField label="Prof" source="prof" reference="profs" link="show">
      <FunctionField
        label="Prof"
        render={(prof) => `${prof.firstname} ${prof.lastname}`}
      />
    </ReferenceField>

    <ReferenceField label="Eleve" source="eleve" reference="eleves" link="show">
      <FunctionField
        label="Eleve"
        render={(eleve) => `${eleve.firstname} ${eleve.lastname}`}
      />
    </ReferenceField>
  </ShowGuesser>
);

const UserEdit = (props) => (
  <EditGuesser {...props}>
    <InputGuesser source={"email"} />

    <SelectArrayInput
      label="Roles"
      source="roles"
      choices={[
        { id: "ROLE_USER", name: "ROLE_USER" },
        { id: "ROLE_ELEVE", name: "ROLE_ELEVE" },
        { id: "ROLE_PROF", name: "ROLE_PROF" },
        { id: "ROLE_ADMIN", name: "ROLE_ADMIN" },
      ]}
    />

    <ReferenceInput
      label="Prof"
      source="prof"
      reference="profs"
      filterToQuery={(searchText) => ({ name: searchText })}
    >
      <AutocompleteInput
        optionText={(prof) => `${prof.firstname} ${prof.lastname}`}
        defaultValue={null}
      />
    </ReferenceInput>

    <ReferenceInput
      label="Eleve"
      source="eleve"
      reference="eleves"
      filterToQuery={(searchText) => ({ name: searchText })}
    >
      <AutocompleteInput
        optionText={(eleve) => `${eleve.firstname} ${eleve.lastname}`}
        defaultValue={null}
      />
    </ReferenceInput>
  </EditGuesser>
);

const UserCreate = (props) => (
  <CreateGuesser {...props}>
    <InputGuesser source={"email"} />
    <PasswordInput source="password" />

    <SelectArrayInput
      label="Roles"
      source="roles"
      choices={[
        { id: "ROLE_USER", name: "ROLE_USER" },
        { id: "ROLE_ELEVE", name: "ROLE_ELEVE" },
        { id: "ROLE_PROF", name: "ROLE_PROF" },
        { id: "ROLE_ADMIN", name: "ROLE_ADMIN" },
      ]}
    />

    <ReferenceInput
      label="Prof"
      source="prof"
      reference="profs"
      filterToQuery={(searchText) => ({ name: searchText })}
    >
      <AutocompleteInput
        optionText={(prof) => `${prof.firstname} ${prof.lastname}`}
        defaultValue={null}
      />
    </ReferenceInput>

    <ReferenceInput
      label="Eleve"
      source="eleve"
      reference="eleves"
      filterToQuery={(searchText) => ({ name: searchText })}
    >
      <AutocompleteInput
        optionText={(eleve) => `${eleve.firstname} ${eleve.lastname}`}
        defaultValue={null}
      />
    </ReferenceInput>
  </CreateGuesser>
);

const AdminCustom = () => {
  const { setPath } = useContext(MainContext);

  useEffect(() => {
    setPath(window.location.href);
  });

  return (
    <>
      <HydraAdmin
        basename="/admin"
        entrypoint={entrypoint}
        dataProvider={dataProvider}
      >
        <ResourceGuesser
          name={"users"}
          list={UserList}
          show={UserShow}
          edit={UserEdit}
          create={UserCreate}
        />
        <ResourceGuesser
          name={"profs"}
          list={ProfsList}
          show={ProfShow}
          edit={ProfEdit}
          create={ProfCreate}
        />
        <ResourceGuesser
          name={"eleves"}
          list={ElevesList}
          show={EleveShow}
          edit={EleveEdit}
          create={EleveCreate}
        />
        <ResourceGuesser name={"matieres"} />
        <ResourceGuesser
          name={"cours"}
          list={CoursList}
          show={CoursShow}
          edit={CoursEdit}
          create={CoursCreate}
        />
      </HydraAdmin>
      {/* Solution avec détéction de l'url pour cacher le header dans le dashboard */}
      <a id="admin-back-button" href="/">
        Retour au site
      </a>
    </>
  );
};

export default AdminCustom;
