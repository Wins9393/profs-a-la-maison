import { Link } from "react-router-dom";
import { Button, Menu, Badge } from "antd";
import React, { useContext } from "react";
import "./header.css";
import MainContext from "../../contexts/MainContext";
import {
  HomeOutlined,
  UserOutlined,
  FormOutlined,
  DatabaseOutlined,
  LoginOutlined,
  LogoutOutlined,
} from "@ant-design/icons";

const Header = () => {
  const { currentUser, deconnexion, countCoursNotSeen } =
    useContext(MainContext);

  if (currentUser) {
    if (currentUser.roles.includes("ROLE_USER")) {
      return (
        <div className="header">
          <Menu mode="horizontal" className="header-menu-left">
            <Menu.Item key="home">
              <HomeOutlined />
              <Link to="/" style={{ marginLeft: "8px" }}>
                Accueil
              </Link>
            </Menu.Item>
            <Menu.Item key="profs">
              <FormOutlined />
              <Link to="/profs" style={{ marginLeft: "8px" }}>
                Nos Profs
              </Link>
            </Menu.Item>
            {currentUser.roles.includes("ROLE_ADMIN") ? (
              <Menu.Item key="admin">
                <DatabaseOutlined />
                <Link to="/admin" style={{ marginLeft: "8px" }}>
                  Dashboard
                </Link>
              </Menu.Item>
            ) : (
              ""
            )}
          </Menu>
          <Menu mode="horizontal" className="header-menu-right">
            <Menu.Item key="account">
              <UserOutlined />
              <Badge count={countCoursNotSeen} offset={[10, 0]}>
                <Link to="/account">Mon compte</Link>
              </Badge>
            </Menu.Item>

            <Menu.Item key="logout" className="deconnexion-button">
              <Button type="primary" ghost onClick={deconnexion}>
                <LogoutOutlined />
                Deconnexion
              </Button>
            </Menu.Item>
          </Menu>
        </div>
      );
    }
  } else {
    return (
      <div className="header">
        <Menu mode="horizontal" className="header-menu-left">
          <Menu.Item key="home">
            <HomeOutlined />
            <Link to="/" style={{ marginLeft: "8px" }}>
              Accueil
            </Link>
          </Menu.Item>
          <Menu.Item key="profs">
            <FormOutlined />
            <Link to="/profs" style={{ marginLeft: "8px" }}>
              Nos Profs
            </Link>
          </Menu.Item>
        </Menu>
        <Menu mode="horizontal" className="header-menu-right">
          <Menu.Item key="login">
            <LoginOutlined />
            <Link to="/login" style={{ marginLeft: "8px" }}>
              Connexion
            </Link>
          </Menu.Item>
          <Menu.SubMenu title="Inscription" key="register">
            <Menu.ItemGroup>
              <Menu.Item key="register-eleve">
                <Link to="/register-eleve">En tant qu'élève</Link>
              </Menu.Item>
              <Menu.Item key="register-prof">
                <Link to="/register-prof">En tant que prof</Link>
              </Menu.Item>
            </Menu.ItemGroup>
          </Menu.SubMenu>
        </Menu>
      </div>
    );
  }
};

export default Header;
