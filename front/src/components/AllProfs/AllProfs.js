import { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import MainContext from "../../contexts/MainContext";
import ProfCard from "../ProfCard/ProfCard";
import { Row, Col, Input, Select, Slider, Button } from "antd";
import { SlidersOutlined } from "@ant-design/icons";
import "./allProfs.css";
import { Rings } from "react-loader-spinner";

const { Search } = Input;

const AllProfs = () => {
  const { fetchProfs, profs, matieres, loading } = useContext(MainContext);
  const [profsFiltered, setProfsFiltered] = useState();
  let isVisible = false;

  const { Option } = Select;

  useEffect(() => {
    fetchProfs();
  }, []);

  useEffect(() => {
    toggleFilters();
  });

  useEffect(() => {
    setProfsFiltered(profs);
  }, [profs]);

  const marksNote = {
    0: "0",
    1: "1",
    2: "2",
    3: "3",
    4: "4",
    5: "5",
  };

  const markPrix = {
    0: "0€",
    500: "500€",
  };

  const toggleFilters = () => {
    const blocFilter = document.querySelector(".all-filter-container");
    const toggleFilterButton = document.querySelector(".toggleFiltersButton");

    if (toggleFilterButton) {
      toggleFilterButton.addEventListener("click", () => {
        if (!isVisible) {
          isVisible = true;
          blocFilter.hidden = false;
        } else if (isVisible) {
          isVisible = false;
          blocFilter.hidden = true;
        }
      });
    }
  };

  const findProfsByMatiere = (matiereID) => {
    let profsTmp = [];

    profs.filter((prof) =>
      prof.matiere.map((mat) => {
        if (mat.id == matiereID) {
          profsTmp.push(prof);
        }
      })
    );
    setProfsFiltered(profsTmp);
  };

  const onCitySearch = (value) => {
    const profsTmp = profs.filter((prof) => {
      return prof.city.toLowerCase().includes(value.toLowerCase());
    });
    setProfsFiltered(profsTmp);
  };

  const onSelectSearch = (value) => {
    if (value) {
      const matiereID = value.split("/")[3];
      findProfsByMatiere(matiereID);
    } else {
      setProfsFiltered(profs);
    }
  };

  const onRateSearch = (value) => {
    const profsTmp = profs.filter((prof) => {
      const initialValue = 0;
      const sumNote = prof.note.reduce(
        (prev, curr) => prev + curr,
        initialValue
      );
      return sumNote / prof.note.length >= value;
    });
    setProfsFiltered(profsTmp);
  };

  const onPriceSearch = (value) => {
    const profsTmp = profs.filter((prof) => {
      return prof.pricing >= value;
    });
    setProfsFiltered(profsTmp);
  };

  return loading ? (
    <div className="loader-container">
      <Rings height="100" width="100" color="#fbc531" ariaLabel="loading" />
    </div>
  ) : (
    <div>
      <div className="search-and-title-bloc-container">
        <div className="search-and-title-bloc">
          <h2 className="all-profs-title">Tous les profs</h2>
          <Button className="toggleFiltersButton">
            Filtres <SlidersOutlined />
          </Button>
          <div className="all-filter-container" hidden>
            <div className="search-container">
              <div className="search-city">
                <div>Rechercher par ville</div>
                <Search
                  className="search-bar"
                  placeholder="Choisissez une ville"
                  allowClear
                  enterButton="Rechercher"
                  size="large"
                  onSearch={onCitySearch}
                />
              </div>

              <div className="search-matiere">
                <div>Rechercher par matière</div>
                <Select
                  className="search-bar"
                  placeholder="Choisissez une matière"
                  onChange={onSelectSearch}
                >
                  {matieres.map((matiere) => {
                    return (
                      <Option
                        value={`/api/matieres/${matiere.id}`}
                        key={matiere.id}
                      >
                        {matiere.name}
                      </Option>
                    );
                  })}
                </Select>
              </div>
            </div>
            <div className="search-container-slider">
              <div className="slider-note">
                <div>Rechercher par note</div>
                <Slider
                  min={0}
                  max={5}
                  marks={marksNote}
                  step={null}
                  defaultValue={3}
                  onAfterChange={onRateSearch}
                />
              </div>
              <div className="slider-prix">
                <div>Rechercher par prix</div>
                <Slider
                  min={0}
                  max={500}
                  marks={markPrix}
                  step={5}
                  defaultValue={50}
                  onAfterChange={onPriceSearch}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container-all-profs">
        {profsFiltered ? (
          <Row gutter={[48, 12]} justify="center" align="center">
            {profsFiltered.map(({ ...prof }) => {
              return (
                <Col span={12} key={prof.id}>
                  <Link className="prof-card-in-link" to={`/prof/${prof.id}`}>
                    <ProfCard {...prof} />
                  </Link>
                </Col>
              );
            })}
          </Row>
        ) : (
          <div className="loader-container">
            <Rings
              height="100"
              width="100"
              color="#fbc531"
              ariaLabel="loading"
            />
          </div>
        )}
      </div>
    </div>
  );
};

export default AllProfs;
