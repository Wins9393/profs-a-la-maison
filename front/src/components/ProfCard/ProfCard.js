import { Card, Rate, Avatar } from "antd";
import "./profcard.css";

const ProfCard = ({ ...prof }) => {
  const getAverage = () => {
    if (prof) {
      let average = prof.note ? prof.note.reduce((pv, cv) => pv + cv, 0) : 0;
      return average ? average / prof.note.length : 0;
    }
  };

  return (
    <Card className="prof-card">
      <div className="prof-card-subcontainer">
        <div className="card-avatar-wrapper">
          <Avatar size={128} src={prof.image} />
        </div>

        <div className="name-rate-wrapper">
          <h2 className="prof-card-name">
            {prof.firstname} {prof.lastname}
          </h2>
          <Rate
            allowHalf
            disabled
            defaultValue={getAverage()}
            className="rate-card"
          />
        </div>
        <div className="matiere-wrapper">
          {prof.matiere.length
            ? prof.matiere.map((mat) => {
                return (
                  <h3 key={mat.id} className="prof-card-matiere">
                    {mat.name}
                  </h3>
                );
              })
            : ""}
        </div>
      </div>
    </Card>
  );
};

export default ProfCard;
