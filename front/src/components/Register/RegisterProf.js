import { Form, Input, InputNumber, Button, Select } from "antd";
import { useContext } from "react";
import MainContext from "../../contexts/MainContext";
import { Navigate } from "react-router-dom";

const RegisterProf = () => {
  const { matieres, currentUser } = useContext(MainContext);

  const onFinish = async (values) => {
    const res = await fetch(`${process.env.REACT_APP_API_ENTRYPOINT}/profs`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(values),
    });

    const data = await res.json();

    window.location.replace("/register-user");
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const { Option } = Select;

  if (matieres.length) {
    return !currentUser ? (
      <div className="container-register-form">
        <h2 className="register-title">Inscription prof</h2>
        <Form
          className="register-form"
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          initialValues={{
            roles: [],
            image:
              "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQhaOWldUPHrJ0iaaCNXbgeT4LpQTTF0-U9vk5k-r2Tc946NPTOnUvRnb0GT7e4bMkokOY&usqp=CAU",
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Prénom"
            name="firstname"
            rules={[
              {
                required: true,
                type: "string",
                message: "Please input your firstname!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Nom"
            name="lastname"
            rules={[
              {
                required: true,
                type: "string",
                message: "Please input your lastname!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Email"
            name="email"
            rules={[
              {
                required: true,
                type: "email",
                message: "Please input your email!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Rue"
            name="street"
            rules={[
              {
                required: true,
                type: "string",
                message: "Please input your street!",
              },
            ]}
          >
            <Input placeholder="Street" />
          </Form.Item>

          <Form.Item
            label="Numéro"
            name="streetnumber"
            rules={[
              {
                required: true,
                type: "number",
                message: "Please input your streetnumber!",
              },
            ]}
          >
            <InputNumber style={{ display: "float" }} />
          </Form.Item>

          <Form.Item
            label="Ville"
            name="city"
            rules={[
              {
                required: true,
                type: "string",
                message: "Please input your city!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Code Postal"
            name="zipcode"
            rules={[
              {
                required: true,
                type: "number",
                message: "Please input your zipcode!",
              },
            ]}
          >
            <InputNumber min={0} max={99999} />
          </Form.Item>

          <Form.Item
            label="Matière"
            name="matiere"
            rules={[
              {
                required: true,
                message: "Please select your course",
                type: "array",
              },
            ]}
          >
            <Select mode="multiple" placeholder="Please select your course">
              {matieres.map((matiere) => {
                return (
                  <Option
                    value={`/api/matieres/${matiere.id}`}
                    key={matiere.id}
                  >
                    {matiere.name}
                  </Option>
                );
              })}
            </Select>
          </Form.Item>

          <Form.Item
            label="Prix horaire"
            name="pricing"
            rules={[
              {
                required: true,
                type: "number",
                message: "Please input your pricing!",
              },
            ]}
          >
            <InputNumber min={0} max={99999} />
          </Form.Item>

          <Form.Item
            label="Image"
            name="image"
            rules={[
              {
                required: false,
                type: "string",
                message: "Please input your city!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Description"
            name="description"
            rules={[
              {
                required: true,
                type: "string",
                message: "Please input your description!",
              },
            ]}
          >
            <Input.TextArea rows={6} />
          </Form.Item>

          <Form.Item
            label="Roles"
            name="roles"
            hidden="true"
            rules={[
              {
                required: false,
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Button type="primary" htmlType="submit">
              Valider
            </Button>
          </Form.Item>
        </Form>
      </div>
    ) : (
      <Navigate
        to={{
          pathname: "/",
        }}
      />
    );
  } else {
    return "Chargement...";
  }
};

export default RegisterProf;
