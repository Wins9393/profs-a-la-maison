import { Form, Input, Button } from "antd";
import { useContext, useEffect } from "react";
import MainContext from "../../contexts/MainContext";
import { Navigate } from "react-router-dom";
import InfoMessages from "../Account/InfoMessages";

const RegisterUser = () => {
  const {
    fetchProfs,
    fetchEleves,
    profs,
    eleves,
    currentUser,
    message,
    setMessage,
  } = useContext(MainContext);

  useEffect(() => {
    fetchProfs();
    fetchEleves();
  }, []);

  const onFinish = async (values) => {
    let userType = "";

    eleves.map((eleve) => {
      if (eleve.email === values.email) {
        userType = eleve;
      }
    });

    profs.map((prof) => {
      if (prof.email === values.email) {
        userType = prof;
      }
    });

    if (userType.matiere) {
      try {
        const res = await fetch(
          `${process.env.REACT_APP_API_ENTRYPOINT}/users`,
          {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              email: values.email,
              roles: ["ROLE_PROF"],
              password: values.password,
              prof: `/api/profs/${userType.id}`,
            }),
          }
        );
        const data = await res.json();

        window.location.replace("/");
      } catch (error) {
        setMessage({
          error: "Une erreur est survenue lors de votre inscription",
        });
        throw error;
      }
    } else {
      try {
        const res = await fetch(
          `${process.env.REACT_APP_API_ENTRYPOINT}/users`,
          {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              email: values.email,
              roles: ["ROLE_ELEVE"],
              password: values.password,
              eleve: `/api/eleves/${userType.id}`,
            }),
          }
        );
        const data = await res.json();

        if (!data.title) window.location.replace("/");
        else
          setMessage({
            error:
              "Vous devez créer un compte prof ou élève avant cette étape !\nSi vous disposez déjà d'un compte, renseignez votre adresse email et définissez un mot de passe.",
          });
      } catch (error) {
        setMessage({
          error: "Vous devez créer un compte prof ou élève avant cette étape !",
        });
        throw error;
      }
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return !currentUser ? (
    <div className="container-register-form">
      <h2 className="register-title">Création du compte</h2>
      <Form
        className="register-form"
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        initialValues={{
          roles: [],
          image:
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQhaOWldUPHrJ0iaaCNXbgeT4LpQTTF0-U9vk5k-r2Tc946NPTOnUvRnb0GT7e4bMkokOY&usqp=CAU",
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="Email"
          name="email"
          rules={[
            {
              required: true,
              type: "email",
              message: "Please input your email!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          label="Roles"
          name="roles"
          hidden="true"
          rules={[
            {
              required: false,
            },
          ]}
        >
          <Input />
        </Form.Item>

        {message ? <InfoMessages mess={message} /> : ""}

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  ) : (
    <Navigate
      to={{
        pathname: "/",
      }}
    />
  );
};

export default RegisterUser;
