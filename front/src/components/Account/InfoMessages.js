import { CheckCircleOutlined, CloseCircleOutlined } from "@ant-design/icons";

const InfoMessages = (mess) => {
  return mess.mess.success ? (
    <div
      className={`display-message display-message-success ${
        mess.mess.success.includes("Votre rendez-vous le")
          ? "display-message-success-rdv"
          : ""
      }`}
    >
      <p>
        <CheckCircleOutlined /> {mess.mess.success}
      </p>
    </div>
  ) : mess.mess.error ? (
    <div
      className={`display-message display-message-error ${
        mess.mess.error ===
        "Vous devez être connecté avec un compte élève pour prendre rendez-vous"
          ? "display-message-error-rdv"
          : ""
      }`}
    >
      <p>
        <CloseCircleOutlined /> {mess.mess.error}
      </p>
    </div>
  ) : (
    ""
  );
};

export default InfoMessages;
