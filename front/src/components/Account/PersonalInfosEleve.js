import React, { useContext, useEffect } from "react";
import { Form, Input, InputNumber, Button } from "antd";
import InfoMessages from "./InfoMessages";
import MainContext from "../../contexts/MainContext";

const PersonalInfosEleve = (currentUserAll) => {
  const { message, setMessage } = useContext(MainContext);
  const [form] = Form.useForm();

  useEffect(() => {
    setMessage({});
  }, []);

  const onFinish = async (values) => {
    try {
      const res = await fetch(
        `${process.env.REACT_APP_API_ENTRYPOINT}/eleves/${currentUserAll.currentUserAll.eleve.id}`,
        {
          method: "PATCH",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/merge-patch+json",
          },
          body: JSON.stringify(values),
        }
      );
      const data = await res.json();

      setMessage({ success: "Informations modifiées avec succès !" });
    } catch (err) {
      setMessage({ error: "Echec de la modification !" });
      throw err;
    }
  };

  const onFinishFailed = () => {
    setMessage({ error: "Echec de la modification !" });
  };

  return (
    <Form
      layout="horizontal"
      className="personal-infos-form"
      form={form}
      name="register"
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      initialValues={{
        firstname: currentUserAll.currentUserAll.eleve.firstname,
        lastname: currentUserAll.currentUserAll.eleve.lastname,
        streetnumber: currentUserAll.currentUserAll.eleve.streetnumber,
        street: currentUserAll.currentUserAll.eleve.street,
        city: currentUserAll.currentUserAll.eleve.city,
        zipcode: currentUserAll.currentUserAll.eleve.zipcode,
      }}
      scrollToFirstError
    >
      <Form.Item
        name="firstname"
        label="Prénom"
        // tooltip="What do you want others to call you?"
        rules={[
          {
            required: true,
            message: "Please input your firstname!",
            type: "string",
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        name="lastname"
        label="Nom"
        // tooltip="What do you want others to call you?"
        rules={[
          {
            required: true,
            message: "Please input your lastname!",
            type: "string",
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Numéro de rue"
        name="streetnumber"
        rules={[
          {
            required: true,
            type: "number",
            message: "Please input your streetnumber!",
          },
        ]}
      >
        <InputNumber style={{ display: "float" }} />
      </Form.Item>

      <Form.Item
        label="Rue"
        tooltip="Intitulé complet de la rue. Ex: Rue de la république"
        name="street"
        rules={[
          {
            required: true,
            type: "string",
            message: "Please input your street!",
          },
        ]}
      >
        <Input placeholder="Street" />
      </Form.Item>

      <Form.Item
        label="Ville"
        name="city"
        rules={[
          {
            required: true,
            type: "string",
            message: "Please input your city!",
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Code Postal"
        name="zipcode"
        rules={[
          {
            required: true,
            type: "number",
            message: "Please input your zipcode!",
          },
        ]}
      >
        <InputNumber min={0} max={99999} />
      </Form.Item>

      {message ? <InfoMessages mess={message} /> : ""}

      <Form.Item className="container-btn">
        <Button className="btn-submit" type="primary" htmlType="submit">
          Modifier
        </Button>
      </Form.Item>
    </Form>
  );
};

export default PersonalInfosEleve;
