import { useContext, useEffect, useState } from "react";
import MainContext from "../../contexts/MainContext";
import PersonalInfosProf from "./PersonalInfosProf";
import PersonalInfosEleve from "./PersonalInfosEleve";
import PersonalCours from "./PersonalCours";
import { Menu } from "antd";
import "./Account.css";
import { Rings } from "react-loader-spinner";

const Account = () => {
  const { currentUserAll, setCurrentUserAll, loading, setLoading } =
    useContext(MainContext);
  const [token, setToken] = useState(null);
  const [contentToDisplay, setContentToDisplay] = useState();
  const [setMessage] = useState({});
  const [stripeAccount, setStripeAccount] = useState(null);

  useEffect(() => {
    getToken();
    fetchCurrentUser();
  }, [token, contentToDisplay]);

  useEffect(() => {
    getStripeAccount();
  }, [currentUserAll]);

  const getToken = () => {
    if (localStorage.getItem("currentUser")) {
      setToken(JSON.parse(localStorage.getItem("currentUser")).token);
    }
  };

  const fetchCurrentUser = async () => {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_ENTRYPOINT}/me`,
        {
          method: "GET",
          headers: {
            Authorization: `Bearer ${token}`,
            Accept: "application/json",
            "Content-Type": "application/json",
          },
        }
      );

      const data = await response.json();

      if (data.id) {
        setCurrentUserAll(data);
        setLoading(false);
      }

      if (currentUserAll !== []) {
        if (data.message === "Expired JWT Token") {
          localStorage.removeItem("currentUser");
          setCurrentUserAll(null);
          setMessage({ error: "Token expiré, veuillez vous reconnecter !" });
          window.location.replace("/");
        }
      }
    } catch (err) {
      throw err;
    }
  };

  const showInfos = () => {
    setContentToDisplay("infos");
  };

  const showCours = () => {
    setContentToDisplay("cours");
  };

  const createStripeAccount = async () => {
    // if (currentUserAll && stripeAccount != null && !stripeAccount.length)
    // window.location.href = `https://localhost:8000/create-stripe-account?email=${currentUserAll.email}`;
    window.location.href = `${process.env.REACT_APP_BACK_URL}/create-stripe-account?email=${currentUserAll.email}`;
  };

  // réécrite dans le context
  const getStripeAccount = async () => {
    const response = await fetch(
      `${process.env.REACT_APP_BACK_URL}/stripe-account-infos`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      }
    );
    const data = await response.json();

    const stripeCustomerAccount = data.data.filter(
      (acc) => acc.email === currentUserAll.email
    );
    setStripeAccount(stripeCustomerAccount);
  };

  if (currentUserAll && currentUserAll.eleve) {
    return (
      <div className="account-container">
        <div className="content">
          {contentToDisplay === "infos" ? (
            <div className="container-personal-infos-form">
              <h2 className="title-h2">Vos informations personnelles</h2>
              <PersonalInfosEleve currentUserAll={currentUserAll} />
            </div>
          ) : (
            <PersonalCours currentUserAll={currentUserAll} />
          )}
        </div>
        <div className="sidebar">
          <Menu>
            <Menu.Item key="cours" onClick={showCours}>
              Mes cours
            </Menu.Item>
            <Menu.Item key="infos" onClick={showInfos}>
              Mes informations
            </Menu.Item>
          </Menu>
        </div>
      </div>
    );
  } else if (currentUserAll && currentUserAll.prof) {
    return (
      <div className="account-container">
        <div className="content">
          {contentToDisplay === "infos" ? (
            <div className="container-personal-infos-form">
              <h2 className="title-h2">Vos informations personnelles</h2>
              <PersonalInfosProf currentUserAll={currentUserAll} />
            </div>
          ) : (
            <PersonalCours currentUserAll={currentUserAll} />
          )}
        </div>
        <div className="sidebar">
          <Menu>
            <Menu.Item key="cours" onClick={showCours}>
              Mes cours
            </Menu.Item>
            <Menu.Item key="infos" onClick={showInfos}>
              Mes informations
            </Menu.Item>
            {currentUserAll &&
            stripeAccount != null &&
            !stripeAccount.length ? (
              <Menu.Item
                key="create-stripe-account"
                style={{ backgroundColor: "#1e90ff", color: "white" }}
                onClick={createStripeAccount}
              >
                Recevoir des paiements
              </Menu.Item>
            ) : (
              ""
            )}
          </Menu>
        </div>
      </div>
    );
  } else {
    return (
      <div className="loader-container">
        <Rings height="100" width="100" color="#fbc531" ariaLabel="loading" />
      </div>
    );
  }
};

export default Account;
