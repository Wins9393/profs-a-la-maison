import React, { useEffect, useContext } from "react";
import MainContext from "../../contexts/MainContext";
import { Form, Input, InputNumber, Select, Button } from "antd";
import InfoMessages from "./InfoMessages";

const { Option } = Select;

const PersonalInfosProf = (currentUserAll) => {
  const { matieres, message, setMessage } = useContext(MainContext);
  const [form] = Form.useForm();

  useEffect(() => {
    setMessage({});
  }, []);

  const onFinish = async (values) => {
    try {
      const res = await fetch(
        `${process.env.REACT_APP_API_ENTRYPOINT}/profs/${currentUserAll.currentUserAll.prof.id}`,
        {
          method: "PATCH",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/merge-patch+json",
          },
          body: JSON.stringify(values),
        }
      );

      const data = await res.json();

      setMessage({ success: "Informations modifiées avec succès !" });
    } catch (err) {
      setMessage({ error: "Echec de la modification !" });
      throw err;
    }
  };

  const onFinishFailed = () => {
    setMessage({ error: "Echec de la modification !" });
  };

  const onImageChange = (cv, av) => {
    const prevAvatar = document.querySelector("#prevAvatar");
    if (cv.image) {
      prevAvatar.src = cv.image;
    }
  };

  return (
    <Form
      layout="horizontal"
      className="personal-infos-form"
      form={form}
      name="register"
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      onValuesChange={onImageChange}
      initialValues={{
        firstname: currentUserAll.currentUserAll.prof.firstname,
        lastname: currentUserAll.currentUserAll.prof.lastname,
        streetnumber: currentUserAll.currentUserAll.prof.streetnumber,
        street: currentUserAll.currentUserAll.prof.street,
        city: currentUserAll.currentUserAll.prof.city,
        zipcode: currentUserAll.currentUserAll.prof.zipcode,
        matiere: currentUserAll.currentUserAll.prof.matiere,
        pricing: currentUserAll.currentUserAll.prof.pricing,
        image: currentUserAll.currentUserAll.prof.image,
        description: currentUserAll.currentUserAll.prof.description,
      }}
      scrollToFirstError
    >
      <Form.Item
        name="firstname"
        label="Prénom"
        // tooltip="What do you want others to call you?"
        rules={[
          {
            required: true,
            message: "Please input your firstname!",
            type: "string",
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        name="lastname"
        label="Nom"
        // tooltip="What do you want others to call you?"
        rules={[
          {
            required: true,
            message: "Please input your lastname!",
            type: "string",
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Numéro de rue"
        name="streetnumber"
        rules={[
          {
            required: true,
            type: "number",
            message: "Please input your streetnumber!",
          },
        ]}
      >
        <InputNumber style={{ display: "float" }} />
      </Form.Item>

      <Form.Item
        label="Rue"
        tooltip="Intitulé complet de la rue. Ex: Rue de la république"
        name="street"
        rules={[
          {
            required: true,
            type: "string",
            message: "Please input your street!",
          },
        ]}
      >
        <Input placeholder="Street" />
      </Form.Item>

      <Form.Item
        label="Ville"
        name="city"
        rules={[
          {
            required: true,
            type: "string",
            message: "Please input your city!",
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Code Postal"
        name="zipcode"
        rules={[
          {
            required: true,
            type: "number",
            message: "Please input your zipcode!",
          },
        ]}
      >
        <InputNumber min={0} max={99999} />
      </Form.Item>

      <Form.Item
        label="Matières"
        name="matiere"
        rules={[
          {
            required: true,
            message: "Please select your courses",
            type: "array",
          },
        ]}
      >
        <Select mode="multiple" placeholder="Please select your courses">
          {matieres.map((matiere) => {
            return (
              <Option value={`/api/matieres/${matiere.id}`} key={matiere.id}>
                {matiere.name}
              </Option>
            );
          })}
        </Select>
      </Form.Item>

      <Form.Item
        label="Prix horaire"
        tooltip="Votre prix à l'heure"
        name="pricing"
        rules={[
          {
            required: true,
            type: "number",
            message: "Please input your pricing!",
          },
        ]}
      >
        <InputNumber min={0} max={99999} />
      </Form.Item>

      <Form.Item
        label="Description"
        name="description"
        rules={[
          {
            required: true,
            type: "string",
            message: "Please input your description!",
          },
        ]}
      >
        <Input.TextArea rows={6} />
      </Form.Item>

      <Form.Item
        label="Image"
        name="image"
        rules={[
          {
            required: false,
            type: "string",
            message: "Please input your image!",
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item name="previewImage">
        <img
          id="prevAvatar"
          width="256"
          title="Aperçu de votre image de profil"
          src={currentUserAll.currentUserAll.prof.image}
          alt="image de profil"
        ></img>
      </Form.Item>

      {message ? <InfoMessages mess={message} /> : ""}

      <Form.Item className="container-btn">
        <Button className="btn-submit" type="primary" htmlType="submit">
          Modifier
        </Button>
      </Form.Item>
    </Form>
  );
};

export default PersonalInfosProf;
