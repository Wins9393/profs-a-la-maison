import { useContext, useEffect, useState } from "react";
import MainContext from "../../contexts/MainContext";
import { Button, Collapse, Divider, Rate } from "antd";
import moment from "moment";
import { Rings } from "react-loader-spinner";

const { Panel } = Collapse;

const PersonalCours = (currentUserAll) => {
  const {
    cours,
    fetchCours,
    coursUser,
    setCoursUser,
    loading,
    isSeen,
    setIsSeen,
  } = useContext(MainContext);
  let eleve;
  let prof;
  let coursParUser = [];
  const [isNoted, setIsNoted] = useState(false);

  if (currentUserAll.currentUserAll.eleve) {
    eleve = currentUserAll.currentUserAll;
  } else if (currentUserAll.currentUserAll.prof) {
    prof = currentUserAll.currentUserAll;
  }

  useEffect(() => {
    fetchCours();
    setIsNoted(false);
  }, [isNoted]);

  useEffect(() => {
    setIsSeen(false);
  }, [isSeen]);

  useEffect(() => {
    getCoursParUser();
  }, [cours]);

  const saveNote = async (value, c) => {
    const newAverage = c.prof.note;
    newAverage.push(value);

    try {
      const responseProf = await fetch(
        `${process.env.REACT_APP_API_ENTRYPOINT}/profs/${c.prof.id}`,
        {
          method: "PATCH",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/merge-patch+json",
          },
          body: JSON.stringify({ note: newAverage }),
        }
      );

      const responseCours = await fetch(
        `${process.env.REACT_APP_API_ENTRYPOINT}/cours/${c.id}`,
        {
          method: "PATCH",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/merge-patch+json",
          },
          body: JSON.stringify({ isNoted: true }),
        }
      );

      const dataProf = await responseProf.json();

      const dataCours = await responseCours.json();
      setIsNoted(dataCours.isNoted);
    } catch (error) {
      throw error;
    }
  };

  const getCoursParUser = () => {
    cours.map((cour) => {
      if (eleve) {
        if (eleve.eleve.id === cour.eleve.id) {
          coursParUser.push(cour);
        }
      } else if (prof) {
        if (prof.prof.id === cour.prof.id) {
          coursParUser.push(cour);
        }
      }
    });
    setCoursUser(coursParUser);
  };

  const setIsSeenToDb = async (id) => {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_ENTRYPOINT}/cours/${id}`,
        {
          method: "PATCH",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/merge-patch+json",
          },
          body: JSON.stringify({ isSeen: true }),
        }
      );
      const data = await response.json();
      setIsSeen(data.isSeen);
    } catch (error) {
      throw error;
    }
  };

  const setIsSeenCours = async (key) => {
    if (prof) {
      cours.map((c) => {
        if (key == c.id && c.isSeen === false) {
          setIsSeenToDb(c.id);
        }
      });
    }
  };

  const sendMail = (email) => {
    window.location.href = `mailto:${email}`;
  };

  return loading ? (
    <div className="loader-container">
      <Rings height="100" width="100" color="#fbc531" ariaLabel="loading" />
    </div>
  ) : coursUser.length ? (
    <div className="container-cours">
      <Collapse accordion className="cours-item" onChange={setIsSeenCours}>
        {coursUser.map((c) => {
          return prof ? (
            <Panel
              style={
                c.isSeen == false
                  ? { backgroundColor: "pink" }
                  : { backgroundColor: "white" }
              }
              key={c.id}
              header={
                Math.sign(moment.duration(moment().diff(moment(c.date)))) ===
                -1 ? (
                  <div className="header-cours-accordeon">
                    {moment(c.date).format("LLLL")}{" "}
                    <div className="container-status-cours-a-venir">
                      <p className="status">A venir</p>
                      <div className="futur"></div>
                    </div>
                  </div>
                ) : (
                  <div className="header-cours-accordeon">
                    {moment(c.date).format("LLLL")}{" "}
                    <div className="container-status-cours-passe">
                      <p className="status">Passé</p>
                      <div className="passe"></div>
                    </div>
                  </div>
                )
              }
            >
              <div className="content-cours-container">
                <h3 className="flex-center">
                  Cours de {c.matiere.name} programmé avec {c.eleve.firstname}{" "}
                  {c.eleve.lastname}
                </h3>
                <Button
                  className="btn-contact"
                  size="large"
                  onClick={() => sendMail(c.eleve.email)}
                >
                  Contacter {c.eleve.firstname}
                </Button>
              </div>
            </Panel>
          ) : (
            <Panel
              key={c.id}
              header={
                Math.sign(moment.duration(moment().diff(moment(c.date)))) ===
                -1 ? (
                  <div className="header-cours-accordeon">
                    {moment(c.date).format("LLLL")}{" "}
                    <div className="container-status-cours-a-venir">
                      <p className="status">A venir</p>
                      <div className="futur"></div>
                    </div>
                  </div>
                ) : (
                  <div className="header-cours-accordeon">
                    {moment(c.date).format("LLLL")}{" "}
                    <div className="container-status-cours-passe">
                      <p className="status">Passé</p>
                      <div className="passe"></div>
                    </div>
                  </div>
                )
              }
            >
              <div className="content-cours-container">
                <h3 className="flex-center">
                  Cours de {c.matiere.name} programmé avec{" "}
                  <a href={`/prof/${c.prof.id}`} style={{ marginLeft: 6 }}>
                    {c.prof.firstname} {c.prof.lastname}
                  </a>
                </h3>
                <Button
                  className="btn-contact"
                  size="large"
                  onClick={() => sendMail(c.prof.email)}
                >
                  Contacter {c.prof.firstname}
                </Button>
              </div>

              {!c.isNoted &&
              Math.sign(moment.duration(moment().diff(moment(c.date)))) !==
                -1 ? (
                <div className="container-personal-infos-form">
                  <Divider />
                  <p style={{ marginBottom: -6 }}>
                    Evaluez {c.prof.firstname}{" "}
                  </p>
                  <Rate
                    className="rateCours"
                    allowHalf
                    onChange={(value) => {
                      saveNote(value, c);
                    }}
                  />
                </div>
              ) : (
                ""
              )}
            </Panel>
          );
        })}
      </Collapse>
    </div>
  ) : (
    <div className="pas-de-cours-container">
      <h2 className="title-h2">Vous n'avez pas encore de cours</h2>
    </div>
  );
};

export default PersonalCours;
