import { useEffect, useState } from "react";
import { Rings } from "react-loader-spinner";
import "./checkout.css";

const SuccessUrl = () => {
  const [sessionData, setSessionData] = useState();
  const [nextCours, setNextCours] = useState();

  const url = window.location.search;
  const urlParams = new URLSearchParams(url);
  const sessionId = urlParams.get("session_id");

  useEffect(() => {
    getOrderInfo();
    getNextCours();
  }, []);

  useEffect(() => {
    setCoursToDb();
  }, [sessionData]);

  const getOrderInfo = async () => {
    if (sessionId) {
      const response = await fetch(
        `${process.env.REACT_APP_BACK_URL}/order-info?session_id=${sessionId}`
        // `https://localhost:8000/order-info?session_id=${sessionId}`
      );

      const data = await response.json();
      setSessionData(data[0]);
    }
  };

  const getNextCours = () => {
    if (localStorage.getItem("nextCours") !== "")
      setNextCours(JSON.parse(localStorage.getItem("nextCours")));
  };

  const setCoursToDb = async () => {
    if (
      sessionData &&
      sessionData.payment_status === "paid" &&
      nextCours !== ""
    ) {
      try {
        const response = await fetch(
          `${process.env.REACT_APP_API_ENTRYPOINT}/cours`,
          {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
            },
            body: JSON.stringify(nextCours),
          }
        );

        const data = await response.json();
        localStorage.setItem("nextCours", "");
      } catch (error) {
        throw error;
      }
    }
  };

  return sessionData && sessionData.payment_status === "paid" ? (
    <>
      <div className="summary-order-container">
        <h1 className="title-summary-order">Merci pour votre commande !</h1>
        <div className="summary-order-details">
          <div className="details-wrapper">
            <h2 className="subtitle-summary-order">Résumé de la commande:</h2>
            <ul>
              <li>
                Nom:{" "}
                <span className="bold-details">
                  {sessionData.customer_details.name}
                </span>
              </li>
              <li>
                Email:{" "}
                <span className="bold-details">
                  {sessionData.customer_details.email}
                </span>
              </li>
            </ul>
            <ul>
              <li>
                Montant total:{" "}
                <span className="bold-details">
                  {sessionData.amount_total / 100}€
                </span>
              </li>
              <li>
                Status du paiement:{" "}
                {sessionData.payment_status === "paid" ? (
                  <span className="bold-details">"Payé"</span>
                ) : sessionData.payment_status === "unpaid" ? (
                  <span className="bold-details">"Impayé"</span>
                ) : (
                  <span className="bold-details">"En cours"</span>
                )}
              </li>
            </ul>
            <p className="contact-order-message">
              Nous espérons que votre commande vous donnera satisfaction. Pour
              toute demande relative à votre commande veuillez envoyer un email
              à cette adresse: contact@profs-a-dom.com
            </p>
          </div>
        </div>
      </div>
    </>
  ) : (
    <div className="loader-container-component">
      <Rings height="100" width="100" color="#fbc531" ariaLabel="loading" />
    </div>
  );
};

export default SuccessUrl;
