import { createContext, useState, useEffect } from "react";
import Geocode from "react-geocode";
import jwt_decode from "jwt-decode";

const MainContext = createContext({});

Geocode.setApiKey(`${process.env.REACT_APP_MAP_API_KEY}`);
Geocode.setLanguage("fr");
Geocode.setRegion("fr");
Geocode.setLocationType("ROOFTOP");
Geocode.enableDebug();

const Provider = ({ children }) => {
  const [matieres, setMatieres] = useState([]);
  const [currentUser, setCurrentUser] = useState(null);
  const [currentUserAll, setCurrentUserAll] = useState([]);
  const [profs, setProfs] = useState([]);
  const [profsCoords, setProfsCoords] = useState([]);
  const [profById, setProfById] = useState([]);
  const [eleves, setEleves] = useState([]);
  const [message, setMessage] = useState({});
  const [cours, setCours] = useState([]);
  const [average, setAverage] = useState(0);
  const [loading, setLoading] = useState(true);
  const [coursUser, setCoursUser] = useState([]);
  const [path, setPath] = useState();
  const [token, setToken] = useState(null);
  const [countCoursNotSeen, setCountCoursNotSeen] = useState(0);
  const [isSeen, setIsSeen] = useState(false);
  const [coursLoaded, setCoursLoaded] = useState(false);
  const [stripeId, setStripeId] = useState(null);

  useEffect(() => {
    getCurrentUser();
    getMatieres();
  }, []);

  // Solution avec détéction de l'url pour cacher le header dans le dashboard
  useEffect(() => {
    setPath(window.location.href);
    getToken();
  });

  useEffect(() => {
    fetchCurrentUser();
  }, [token, currentUser]);

  useEffect(() => {
    setCoursLoaded(false);
    fetchCours();
  }, [currentUserAll, isSeen, countCoursNotSeen]);

  useEffect(() => {
    hasNotSeenCours();
  }, [coursLoaded, isSeen]);

  const getToken = () => {
    if (localStorage.getItem("currentUser")) {
      setToken(JSON.parse(localStorage.getItem("currentUser")).token);
    }
  };

  const getStripeAccount = async (prof) => {
    try {
      const response = await fetch(
        // "https://localhost:8000/stripe-account-infos",
        `${process.env.REACT_APP_BACK_URL}/stripe-account-infos`,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
        }
      );

      const result = await response.json();
      const data = result.data;

      const stripeAccount = data.filter((acc) => acc.email === prof.email);

      if (stripeAccount.length) {
        const stripeAccountId = stripeAccount[0].id;
        setStripeId(stripeAccountId);
      }
    } catch (error) {
      throw error;
    }
  };

  const fetchCurrentUser = async () => {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_ENTRYPOINT}/me`,
        {
          method: "GET",
          headers: {
            Authorization: `Bearer ${token}`,
            Accept: "application/json",
            "Content-Type": "application/json",
          },
        }
      );

      const data = await response.json();

      if (data.id) {
        setCurrentUserAll(data);
        setLoading(false);
      }

      if (currentUserAll !== []) {
        if (data.message === "Expired JWT Token") {
          localStorage.removeItem("currentUser");
          setCurrentUserAll(null);
          setMessage({ error: "Token expiré, veuillez vous reconnecter !" });
          window.location.replace("/");
        }
      }
    } catch (err) {
      throw err;
    }
  };

  const login = async (values) => {
    try {
      const res = await fetch(`${process.env.REACT_APP_API_ENTRYPOINT}/login`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify(values),
      });

      const data = await res.json();
      const userInfo = jwt_decode(data.token);

      setCurrentUser(userInfo);
      setLoading(false);

      if (data) {
        localStorage.setItem("currentUser", JSON.stringify(data));
      }
    } catch (err) {
      setMessage({ error: "Identifiants inconnus" });
      throw err;
    }
  };

  const getCurrentUser = () => {
    if (localStorage.getItem("currentUser")) {
      setCurrentUser(
        jwt_decode(JSON.parse(localStorage.getItem("currentUser")).token)
      );
    }
  };

  const deconnexion = () => {
    if (currentUser) {
      localStorage.removeItem("currentUser");
      setCurrentUser(null);
      window.location.replace("/");
    }
  };

  const fetchProfs = async () => {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_ENTRYPOINT}/profs`,
        {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
        }
      );

      const data = await response.json();
      setProfs(data);
      setLoading(false);
    } catch (err) {
      throw err;
    }
  };

  const fetchProfById = async (id) => {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_ENTRYPOINT}/profs/${id}`,
        {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
        }
      );

      const data = await response.json();

      setProfById(data);
      setAverage(getAverage(data));
      setLoading(false);
    } catch (err) {
      throw err;
    }
  };

  const getAverage = (prof) => {
    if (prof.note) {
      let average = prof.note.reduce((pv, cv) => pv + cv, 0);
      return average / prof.note.length;
    } else {
      return 0;
    }
  };

  const getProfsCoords = async (profs) => {
    const promises = profs.map(async (prof) => {
      try {
        const response = await Geocode.fromAddress(
          `${prof.streetnumber}, ${prof.street} ${prof.zipcode}`
        );

        const { lat, lng } = response.results[0].geometry.location;
        const coords = { lat: lat, lng: lng };

        prof.show = false;

        let tmpProfPlusCoords = { ...prof, coords };

        return tmpProfPlusCoords;
      } catch (err) {
        throw err;
      }
    });
    const tmpProfsCoords = await Promise.all(promises);
    setProfsCoords(tmpProfsCoords);
  };

  const fetchEleves = async () => {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_ENTRYPOINT}/eleves`,
        {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
        }
      );

      const data = await response.json();

      setEleves(data);
    } catch (err) {
      throw err;
    }
  };

  const getMatieres = async () => {
    try {
      const res = await fetch(
        `${process.env.REACT_APP_API_ENTRYPOINT}/matieres`,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
        }
      );
      const data = await res.json();
      setMatieres(data);
      setLoading(false);
    } catch (error) {
      throw error;
    }
  };

  const fetchCours = async () => {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_ENTRYPOINT}/cours`,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
        }
      );
      const data = await response.json();
      setCours(data);
      setLoading(false);
      setCoursLoaded(true);
    } catch (error) {
      throw error;
    }
  };

  const hasNotSeenCours = () => {
    let countTmp = 0;
    if (currentUserAll && currentUserAll.prof) {
      if (coursLoaded) {
        cours.map((oneCours) => {
          if (currentUserAll.prof.id === oneCours.prof.id) {
            if (oneCours.isSeen === false) {
              countTmp++;
              setCountCoursNotSeen(countTmp);
            } else {
              countTmp = 0;
              setCountCoursNotSeen(countTmp);
            }
          }
        });
      }
    }
  };

  return (
    <MainContext.Provider
      value={{
        currentUser,
        setCurrentUser,
        currentUserAll,
        setCurrentUserAll,
        fetchProfs,
        profs,
        profsCoords,
        profById,
        fetchProfById,
        setProfsCoords,
        getProfsCoords,
        fetchEleves,
        eleves,
        login,
        deconnexion,
        matieres,
        cours,
        message,
        setMessage,
        average,
        getAverage,
        fetchCours,
        loading,
        setLoading,
        coursUser,
        setCoursUser,
        path,
        setPath,
        countCoursNotSeen,
        setCountCoursNotSeen,
        isSeen,
        setIsSeen,
        hasNotSeenCours,
        getStripeAccount,
        stripeId,
        setStripeId,
      }}
    >
      {children}
    </MainContext.Provider>
  );
};

export { Provider };
export default MainContext;
