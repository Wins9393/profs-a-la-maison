import { Navigate } from "react-router-dom";

const AuthGuard = ({ currentUser, children }) => {
  if (!currentUser) {
    return <Navigate to="/login" replace />;
  }
  return children;
};

export default AuthGuard;
