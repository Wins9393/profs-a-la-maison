import { Navigate } from "react-router-dom";

const AdminGuard = ({ currentUser, children }) => {
  if (!currentUser) return <Navigate to="/login" replace />;
  if (!currentUser.roles.includes("ROLE_ADMIN")) {
    return <Navigate to="/login" replace />;
  }
  return children;
};

export default AdminGuard;
