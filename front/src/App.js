import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Account from "./components/Account/Account";
import AuthGuard from "./contexts/AuthGuard";
import AllProfs from "./components/AllProfs/AllProfs";
import { default as HeaderCustom } from "./components/Header/Header";
import Home from "./components/Home/Home";
import LoginForm from "./components/Login/LoginForm";
import RegisterEleve from "./components/Register/RegisterEleve";
import RegisterProf from "./components/Register/RegisterProf";
import RegisterUser from "./components/Register/RegisterUser";
import PageNotFound from "./components/PageNotFound/PageNotFound";
import { useContext } from "react";
import MainContext from "./contexts/MainContext";
import ProfById from "./components/ProfById/ProfById";
import AdminCustom from "./components/Admin/Admin";
import AdminGuard from "./contexts/AdminGuard";
import SuccessUrl from "./components/Checkout/SuccessUrl";
import FooterCustom from "./components/Footer/FooterCustom";
import { Layout } from "antd";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";

const { Footer } = Layout;

const App = () => {
  const { currentUser, path } = useContext(MainContext);

  return (
    <>
      <Layout>
        <BrowserRouter>
          {/* Solution avec détéction de l'url pour cacher le header dans le dashboard */}
          {path ? !path.includes("admin") ? <HeaderCustom /> : "" : ""}
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/profs" element={<AllProfs />} />
            <Route path="/prof/:id" element={<ProfById />} />
            <Route path="/login" element={<LoginForm />} />
            <Route path="/register-eleve" element={<RegisterEleve />} />
            <Route path="/register-prof" element={<RegisterProf />} />
            <Route path="/register-user" element={<RegisterUser />} />
            <Route path="/success" element={<SuccessUrl />} />
            <Route path="/*" element={<PageNotFound />} />

            <Route
              path="/admin/*"
              element={
                <AdminGuard currentUser={currentUser}>
                  <AdminCustom />
                </AdminGuard>
              }
            />
            <Route
              path="/account"
              element={
                <AuthGuard currentUser={currentUser}>
                  <Account />
                </AuthGuard>
              }
            />
          </Routes>
        </BrowserRouter>
        <Footer>
          {/* Solution avec détéction de l'url pour cacher le footer dans le dashboard */}
          {path ? !path.includes("admin") ? <FooterCustom /> : "" : ""}
        </Footer>
      </Layout>
    </>
  );
};

export default App;
